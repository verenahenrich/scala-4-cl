# Scala Programming for Computational Linguistics

This is the public Git repository of the class __"Scala Programming for Computational 
Linguistics"__ at the University of Tübingen in the 16/17 winter term by Verena
Henrich and Thilo Götz. For more general information about the class, please
check the official [course website](http://flowerbird.de/scala4cl/).

In this repository you will find the slides from the class, the sample
code that goes with the slides, and the weekly exercises. As a student,
you will want to clone this repository (instructions below), and regularly get
the latest copy.

## Contents of this repository

The contents of this repository are updated as the course proceeds.
You find three folders in this repository:

* __slides__: We upload all slides to this folder.
* __samples__: This folder contains code examples that go with the slides.
* __exercises__: Here you find the weekly exercises (in subfolder `src/main`)
  and corresponding unit tests (in subfolder `src/test`).

## Setting up your development environment

For the purposes of this class, we will do our Scala development in the
free edition of the [Intellij IDEA](https://www.jetbrains.com/idea/) development environment.

You should install the following tools (in this order):

1. The Java 8 JDK, latest version.
2. Apache Maven
3. Git
4. Intellij IDEA community edition

Operating system specific instructions:

* On the __Mac__, use homebrew to install Maven and Git. Get IDEA from their website.
* On __Linux__, use your distribution’s update mechanism to install Java,
  Maven and Git (e.g., apt-get on Ubuntu). Get IDEA from their website.

Installing Scala for the command line is not strictly necessary. Make
sure to enable Scala language support when installing IDEA.

## Cloning this repository

To get a copy of the repository to your local machine, you have to _clone_ the repository.
This [help page](https://confluence.atlassian.com/bitbucket/create-and-clone-a-repository-800695642.html#Createandclonearepository-cloneStep2:Cloningtherepositorytoyourlocalsystem)
explains how to get the repository cloned to your local machine. It is basically
the command 
`git clone https://verenahenrich@bitbucket.org/verenahenrich/scala-4-cl.git`.

## Configure source directories

Open the project in Intellij and configure `exercises/src/main/scala` and
`exercises/src/test/scala` as source directories:

1. Right-click on `exercises/src/main/scala` -> Mark Directory as -> Sources Root
2. Right-click on `exercises/src/test/scala` -> Mark Directory as -> Test Sources Root

## Using this repository

Once you have a local copy of the repository, you can update it on
the command line with `git pull`. Since you
will not be committing any code to the repository, that's all you really need to
know about Git.

If you don't want to use Git for some reason, you can also just click on the
__Source__ navigation item on the left and browse to files directly, without
having to install Git.

It is very useful to have some Git skills though. If you would like to learn
more about git, you can start with the help pages over on
[github.com](https://help.github.com/categories/bootcamp/). Once you know the
basics, a very useful reference is [this online book](https://git-scm.com/book/en/v2).

## Using Apache Maven

You can use Maven to build your project and to run tests. For now it is
sufficient to know two Maven commands:

1. To compile your source code run `mvn clean compile`.
2. To run all Scala unit tests run `mvn clean test`.

We will learn more about Maven later. If you like to learn
more about it already now, you can start with the help pages on
maven.apache.org/](https://maven.apache.org/).


