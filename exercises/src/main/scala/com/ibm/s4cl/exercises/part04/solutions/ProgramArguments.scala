package com.ibm.s4cl.exercises.part04.solutions

object ProgramArguments {

  val validArguments = "Valid arguments: " +
    "-h/--help, " +
    "--version, " +
    "-l=[language]/--lang=[language], " +
    "-i=[filename]/--input=[filename], " +
    "-p=[num]/--pages=[num]"

  val version = "Version: 1.0"

  def parseArgument(argument: (String, Any)): String = argument match {
    case ("-h" | "--help", _) => validArguments
    case ("--version", _) => version
    case ("-l" | "--lang", language: String) => setLanguage(language)
    case ("-i" | "--input", inputFile: String) => setInputFile(inputFile)
    case ("-p" | "--pages", numPages: Int) => setNumPages(numPages)
    case _ => validArguments
  }

  def setLanguage(language: String): String = language match {
    case "en" => "Setting the language to English"
    case "de" => "Setting the language to German"
    case _ => "Invalid language, allowed values are: 'en' and 'de'"
  }

  def setInputFile(inputFile: String): String = s"Set input file '$inputFile'"

  def setNumPages(numPages: Int): String = numPages match {
    case negative if negative < 0 => "The number of pages has to be positive"
    case num if num >= 30 => "The number of pages has to be below 30"
    case _ => s"Setting the number of pages to $numPages"
  }
}
