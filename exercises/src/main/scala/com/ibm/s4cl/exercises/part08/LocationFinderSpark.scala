package com.ibm.s4cl.exercises.part08

import java.io.File

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

/**
  * After you have done the location finder exercise, do the
  * same thing in Spark. Can you get the same results? Is it
  * faster, or slower? Here, too, use a small subset of the
  * files while developing.
  *
  * Run the code below as is (passing the directory with the
  * input files as argument) to validate that Spark is working.
  */
object LocationFinderSpark {
  
  def main(args: Array[String]): Unit = {

    // Pass the directory with the input files are program parameter.
    val inputDir = new File(args(0))
  
    // Create a local Spark config for testing, setting the number of cores explicitly.
    val sparkConfig = new SparkConf()
      .setMaster("local")
      .setAppName("Location Finder App")
    // Initialize the Spark context
    val sparkContext = new SparkContext(sparkConfig)
  
    // Read the text files from the input directory into a RDD.
    val input: RDD[(String, String)] = sparkContext
      .wholeTextFiles(inputDir.toURI.toString)
  
    // Get the number of documents from the size of the RDD.
    val docCount = input.count()
    
    println(s"Number of documents in RDD: $docCount")
  
  }
  
}
