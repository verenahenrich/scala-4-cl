package com.ibm.s4cl.exercises.part03

import scala.io.Source

class PosLists(treebankFile: String) {

  // a list of tuples representing all word tokens with their POS tags -- as read from file 'treebankFile'
  val listOfWordTokensWithPos: List[(String, String)] =
    List() // TODO: this variable needs to be initialized

  /**
    * Extracts all word tokens that occur for a specified POS tag in file 'treebankFile'.
    *
    * @param pos the POS tag for which to extract all word tokens
    * @return returns the list of word tokens for the specified POS tag
    */
   def getTokensForSpecificPos(pos: String): List[String] =
     List() // TODO: this method needs to be implemented

  /**
    * Extracts the number of word tokens that occur for a specified POS tag in file 'treebankFile'.
    *
    * @param pos the POS tag for which to extract all word tokens
    * @return returns the number of word tokens for the specified POS tag
    */
  def getNumberOfWordTokensForSpecificPos(pos: String): Int =
    -1 // TODO: this method needs to be implemented

  /**
    * Extracts the list of words that occur for a specified POS tag in file 'treebankFile'. The
    * resulting word list does not contain any duplicates and it is sorted alphabetically.
    *
    * @param pos the POS tag for which to extract all words
    * @return returns the list of alphabetically sorted, distinct words for the specified POS tag
    */
  def getDistinctWordListForSpecificPos(pos: String): List[String] =
    List() // TODO: this method needs to be implemented
}
