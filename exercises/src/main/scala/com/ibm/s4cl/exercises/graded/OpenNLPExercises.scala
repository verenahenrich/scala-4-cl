package com.ibm.s4cl.exercises.graded

import OpenNLP.{getPosTags, getSentences, getTokens}

import scala.annotation.tailrec
import scala.io.Source

case class SentenceWithTokens(sentence: Span, tokens: List[PosToken])

object OpenNLPExercises {

  val text = {
    val is = getClass.getResourceAsStream("/pnp.txt")
    Source
      .fromInputStream(is)
      .mkString
  }

  // Exercise1: compute sentences, tokens and POS tags using the OpenNLP APIs.
  // TODO: compute this list
  val sentences: List[SentenceWithTokens] = Nil

  // Get token for span. (helping method)
  def getTokenAsString(token: PosToken): String = token.span.getCoveredText(text)

  // Exercise2: return the list of "sentence span"/"number of token" pairs, in ascending order by
  // number of tokens.
  def getSentencesByTokenCount(): List[(Span, Int)] = {
    //TODO: implement
    Nil
  }
  
  // Exercise: Get Noun-Noun sequences for the entire text.
  def getNounNounCompounds(): List[(Span, Span)] = {
    //TODO: implement
    Nil
  }

  // Exercise: Extract those tokens that are tagged with different POS tags throughout the text;
  // This method should return a map where the keys are the tokens (as strings, not regarding
  // concrete token spans anymore) and the values are corresponding sets of POS tags occurring
  // anywhere in the text for the token; The resulting map should only contain tokens which occur
  // at least for two different POS.
  def getTokensWithMultiplePos(): Map[String, Set[String]] = {
    // TODO: implement
    Map()
  }

  // Exercise: Extract POS trigrams (only within sentences) with corresponding token trigrams;
  // This method should return a map where the keys are three POS tags occurring as a sequence
  // anywhere in the text (sequences only within sentence boundaries, i.e., filter out sentences
  // that consist of less than three tokens) and the values are corresponding sets of token trigrams.
  def getPosTrigramsWithCorrespondingTokens(): Map[(String, String, String), Set[(String, String, String)]] = {
    // TODO: implement
    Map()
  }

}
