package com.ibm.s4cl.exercises.part06.solutions

import scala.annotation.tailrec


final case class Node(c: Char) {
  
  var isFinal: Boolean = false
  
  var dtrs: Array[Node] = Array()
  
  def getOrAddNode(ch: Char): Node = {
    val pos = TrieDictionarySolution.binarySearch(dtrs, ch)
    if (pos >= 0) dtrs(pos)
    else {
      val node = Node(ch)
      dtrs = TrieDictionarySolution.insert(node, -pos - 1, dtrs)
      node
    }
  }
  
  def getNode(ch: Char): Option[Node] = {
    val pos = TrieDictionarySolution.binarySearch(dtrs, ch)
    if (pos >= 0) Some(dtrs(pos))
    else None
  }
  
}

final case class Span(start: Int, end: Int)

class TrieDictionarySolution {
  
  private var root: Node = Node('_') // Ignore character of root node
  
  def addWord(word: String): TrieDictionarySolution = {
    var node = root
    for (c <- word) node = node.getOrAddNode(c)
    node.isFinal = true
    this
  }
  
  def addAll(words: Iterable[String]): TrieDictionarySolution = {
    words.foreach(addWord(_))
    this
  }
  
  private def findAll(start: Int, chars: Array[Char]): List[Span] = {
    @tailrec
    def findAll(nodeOpt: Option[Node], pos: Int, spans: List[Span]): List[Span] = {
      nodeOpt match {
          case None => spans
          case Some(node) =>
            val newSpans = if (node.isFinal) Span(start, pos)::spans else spans
            if (pos >= chars.length) newSpans
            else findAll(node.getNode(chars(pos)), pos + 1, newSpans)
        }
    }
    findAll(Some(root), start, Nil)
  }
  
  def findAll(text: String): List[Span] = {
    (text.length - 1 to 0 by -1)
      .toList
      .par
      .flatMap(i => findAll(i, text.toArray))
      .toList
  }
  
  def find(word: String): Boolean = {
  
    @tailrec
    def find(pos: Int, chars: Array[Char], node: Node): Boolean = {
      if (pos == chars.size) node.isFinal
      else {
        val char = chars(pos)
        node.getNode(char) match {
          case None => false
          case Some(dtr) => find(pos + 1, chars, dtr)
        }
      }
    }
  
    find(0, word.toArray, root)
  }
  
}

object TrieDictionarySolution {
  
  def apply(words: String*): TrieDictionarySolution = {
    words
      .foldLeft(new TrieDictionarySolution())(_.addWord(_))
  }

  def binarySearch(ar: Array[Node], el: Char): Int = {
    
    @tailrec
    def binsearch(start: Int, end: Int): Int = {
      if (start == end) {
        val current = ar(start)
        if (current.c == el) start
        else if (el < current.c) -start - 1
        else -start - 2 // -(start + 1) - 1
      } else {
        val pos = (end + start) / 2
        val current = ar(pos)
        if (current.c == el) {
          pos
        } else if (el < current.c) {
          if (start == pos) -start - 1
          else binsearch(start, pos - 1)
        } else {
          if (pos == end) -end - 2 // -(end + 1) - 1
          else binsearch(pos + 1, end)
        }
      }
    }
    
    if (ar.size == 0) -1
    else binsearch(0, ar.length - 1)
    
  }

  def insert(node: Node, pos: Int, ar: Array[Node]): Array[Node] = {
    val newArray = new Array[Node](ar.size + 1)
    newArray(pos) = node
    System.arraycopy(ar, 0, newArray, 0, pos)
    System.arraycopy(ar, pos, newArray, pos + 1, ar.size - pos)
    newArray
  }
  
  def printSpan(span: Span, text: String): String = {
    val contextSize = 20
    val prefixStart = Math.max(0, span.start - contextSize)
    val prefix = text.substring(prefixStart, span.start)
    val postfixEnd = Math.min(text.size, span.end + contextSize)
    val postfix = text.substring(span.end, postfixEnd)
    s"$prefix>>${text.substring(span.start, span.end)}<<$postfix"
  }
}
