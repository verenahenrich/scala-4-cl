package com.ibm.s4cl.exercises.part05.solutions

import scala.collection.mutable

// A word class; not strictly necessary, could use the strings directly
case class Word(token: String)

// A sentence is a sequence of words, and the original string (for printing)
case class Sentence(words: Seq[Word], sentence: String) {
  def containsWordsInOrder(wrds: Seq[Word]): Boolean = {
    def containsWordsInOrder(inputIndex: Int, localIndex: Int): Boolean = {
      if (inputIndex == wrds.size) true
      else if (localIndex >= words.size) false
      else if (wrds(inputIndex) == words(localIndex))
                              containsWordsInOrder(inputIndex + 1, localIndex + 1)
      else containsWordsInOrder(inputIndex, localIndex + 1)
    }
    containsWordsInOrder(0, 0)
  }
}

class SentenceIndex {

  val sentenceSet = mutable.Set[Sentence]()
  val emptySentenceSet = mutable.Set[Sentence]()

  val wordMap = mutable.Map[Word, mutable.Set[Sentence]]()

  val sentenceSeparatorRegex = "[.?!;:]+"
  val tokenSeparatorRegex = "\\W+"

  // Create an index from a text string
  def createFrom(text: String): Unit = {
    text
      .split(sentenceSeparatorRegex)
      .foreach(sentenceText => {
        val words = sentenceText
          .split(tokenSeparatorRegex)
          .map(Word(_))
        val sentence = Sentence(words, sentenceText)
        sentenceSet += sentence
        words
          .foreach(w => {
            wordMap.get(w) match {
              case None => wordMap.put(w, mutable.Set(sentence))
              case Some(set) => wordMap.put(w, set += sentence)
            }
          })
      })
  }

  // Find all sentences that contain all the words in the input
  def findInternal(words: Seq[String]): Iterable[Sentence] = {
    if (words.size == 0) sentenceSet
    else words
      .map(Word(_))
      .foldLeft(sentenceSet)((set, word) => {
        wordMap.get(word) match {
          case None => emptySentenceSet
          case Some(wordSet) => set & wordSet
        }
      })
  }

  // Find sentences that contain all words, any order
  def find(words: String*): Iterable[Sentence] = {
    findInternal(words)
  }

  // Find sentences that contain all words, same order, with gaps
  def findOrdered(words: String*): Iterable[Sentence] = {
    val internalWords = words.map(Word(_))
    findInternal(words)
      .filter(_.containsWordsInOrder(internalWords))
  }

  // Find sentences that contain all words, same order, no gaps
  def findContiguous(words: String*): Iterable[Sentence] = {
    val internalWords = words.map(Word(_))
    findInternal(words)
      .filter(_.words.containsSlice(internalWords))
  }
}

object SentenceIndex {

  def main(args: Array[String]): Unit = {
    val text = "his pride. his abominable pride. pride his. too much pride. this sentence has nothing."
    val index = new SentenceIndex
    index.createFrom(text)
    println(index.find("pride").size)
    println(index.find("his").size)
    println(index.find("his", "pride").size)
    println(index.findOrdered("his", "pride").size)
    println(index.findContiguous("his", "pride").size)
  }

}
