package com.ibm.s4cl.exercises.part05

import scala.collection.mutable

// A word class; extend as necessary
class Word

// A sentence should contain a sequence of words, and the original string (for printing)
class Sentence

class SentenceIndex {

  // Add fields for keeping the index. Hint: use a map from Words to sets of sentences

  val sentenceSeparatorRegex = "[.?!;:]+"
  val tokenSeparatorRegex = "\\W+"

  // Create an index from a text string
  def createFrom(text: String): Unit = {
    //TODO: implement
  }

  // Find all sentences that contain all the words in the input
  def findInternal(words: Seq[String]): Iterable[Sentence] = {
    //TODO: implement this helper method and use below
    Nil
  }

  // Find sentences that contain all words, any order
  def find(words: String*): Iterable[Sentence] = {
    //TODO: implement this
    Nil
  }

  // Find sentences that contain all words, same order, with gaps
  def findOrdered(words: String*): Iterable[Sentence] = {
    //TODO: implement this
    Nil
  }

  // Find sentences that contain all words, same order, no gaps
  def findContiguous(words: String*): Iterable[Sentence] = {
    //TODO: implement this
    Nil
  }
}

object SentenceIndex {

  def main(args: Array[String]): Unit = {
    val text = "his pride. his abominable pride. pride his. too much pride. this sentence has nothing."
    val index = new SentenceIndex
    index.createFrom(text)
    println(index.find("pride").size)
    println(index.find("his").size)
    println(index.find("his", "pride").size)
    println(index.findOrdered("his", "pride").size)
    println(index.findContiguous("his", "pride").size)
  }

}
