package com.ibm.s4cl.exercises.graded.solutions

import scala.annotation.tailrec

object MixedExercises {

  // Exercise 1: rewrite the following recursive 'listSize' function in a tail-recursive way;
  //             make sure your code contains the '@tailrec' annotation
  def listSize(list: List[_]): Int = list match {
      case Nil => 0
      case _::tail => 1 + listSize(tail)
    }

  // Solution:
  def listSizeTailRec(list: List[_]): Int = {
    @tailrec
    def listSize(list: List[_], acc: Int): Int = {
      list match {
        case Nil => acc
        case _ :: tail => listSize(tail, acc + 1)
      }
    }
    listSize(list, 0)
  }

  // Exercise 2: write a higher-order function argswitch that switches the arguments of any
  // two-argument function. That is, for any appropriate values a and b, f(a,b) = argswitch(f)(b, a).

  // Solution:
  def argswitch[A, B, C](f: (A, B) => C): (B, A) => C = {
    (b: B, a: A) => f(a,b)
  }
  
  
}
