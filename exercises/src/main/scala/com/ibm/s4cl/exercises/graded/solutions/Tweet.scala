package com.ibm.s4cl.exercises.graded.solutions

sealed trait Tweet {
  def getAuthor: TweetAuthor
  def getText: String
  def getPostedTime: String
  def isRetweet: Boolean
  def getHashtags: List[String]
}

/**
  * Represents an original Tweet.
  * @param author the Tweet's author
  * @param retweetCount the number of times this Tweet has been retweeted
  * @param favoritesCount a number of times this Tweet has been marked as 'favorite'
  * @param text the Tweet's text
  * @param postedTime the time the Tweet has been created
  * @param hashtags a list of hashtags extracted from the text field. Hashtags are those tokens
  *                 that start with a #-symbol, e.g., #scala, #java, etc. Convert hashtags to
  *                 lower case.
  */
case class OriginalTweet(author: TweetAuthor,
                         retweetCount: Int,
                         favoritesCount: Int,
                         text: String,
                         postedTime: String,
                         hashtags: List[String]
                        ) extends Tweet {

  override def getAuthor: TweetAuthor = author
  override def getText: String = text
  override def getPostedTime: String = postedTime
  override def isRetweet: Boolean = false
  override def getHashtags: List[String] = hashtags
}

/**
  * Represents a retweet, i.e., a Tweet that have been retweeted.
  * @param author the retweeter
  * @param text the Tweet's text
  * @param postedTime the time the Tweet has been retweeted
  * @param hashtags a list of hashtags extracted from the text field. Hashtags are those tokens
  *                 that start with a #-symbol, e.g., #scala, #java, etc. Convert hashtags to
  *                 lower case.
  */
case class Retweet(author: TweetAuthor,
                   text: String,
                   postedTime: String,
                   hashtags: List[String]
                  ) extends Tweet {

  override def getAuthor: TweetAuthor = author
  override def getText: String = text
  override def getPostedTime: String = postedTime
  override def isRetweet: Boolean = true
  override def getHashtags: List[String] = hashtags
}

/**
  * Represents a Tweet author.
  * @param name the author's name
  * @param handle the author's handle (unique reference string for an author)
  */
case class TweetAuthor(name: String, handle: String)
