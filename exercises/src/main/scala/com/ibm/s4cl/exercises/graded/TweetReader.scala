package com.ibm.s4cl.exercises.graded

import scala.io.Source

class TweetReader(twitterData: Source) {

  // An (incomplete) set of delimiter characters
  val delimiter = "[ \t\n\r,.…?!\\-:;()\\[\\]'\"/*&$]+"

  // represents a list of all Tweets as read from file `Twitter_data.txt`
  val tweets: List[Tweet] = Nil // TODO initialize

  // originalTweets: represents a list of all original Tweets as read from file `Twitter_data.txt`
  // retweets: represents a list of all retweets as read from file `Twitter_data.txt`
  val (originalTweets, retweets): (List[OriginalTweet], List[Retweet]) = (Nil, Nil) // TODO initialize

  // represents the set of all Tweet's authors (authors from both original Tweets and retweets)
  val authors: Set[TweetAuthor] = Set() // TODO initialize

  /**
    * Extracts all Tweets for the given TweetAuthor.
    * @param author the TweetAuthor for whom to extract all Tweets.
    * @return the list of Tweets for the given TweetAuthor.
    */
  def getTweetsForAuthor(author: TweetAuthor): List[Tweet] = Nil // TODO implement

  /**
    * Extracts all Tweets for all given TweetAuthors.
    * @param authors a Set of TweetAuthors for whom to extract all Tweets.
    * @return all Tweets for the given TweetAuthors.
    */
  def getTweetsForAuthors(authors: Set[TweetAuthor]): Set[Tweet] = Set() // TODO implement

  /**
    * Extracts all Tweets in which the given TweetAuthor is mentioned. An author is mentioned
    * in a Tweet means that the author's handle -- preceded by an @-symbol -- occurs in the Tweet's
    * text. For example, "This is a sample text mentioning @scala_lang." contains @scala_lang, i.e.,
    * it mentions author TweetAuthor("Scala", "scala_lang").
    * @param author the TweetAuthor for whom to extract all Tweets where he/she is mentioned.
    * @return the list of Tweets where the given TweetAuthor is mentioned.
    */
  def getTweetsWhereAuthorIsMentioned(author: TweetAuthor): List[Tweet] = Nil // TODO implement

  /**
    * Extracts all Tweets in which at least one of the given TweetAuthors is mentioned. An author is
    * mentioned in a Tweet means that the author's handle -- preceded by an @-symbol -- occurs in the
    * Tweet's text. For example, "This is a sample text mentioning @scala_lang." contains @scala_lang,
    * i.e., it mentions author TweetAuthor("Scala", "scala_lang").
    * @param authors the TweetAuthors for whom to extract all Tweets where at least one of them is mentioned.
    * @return the list of Tweets where the given TweetAuthors are mentioned.
    */
  def getTweetsWhereAuthorsAreMentioned(authors: Set[TweetAuthor]): Set[Tweet] = Set() // TODO implement

  // TODO implement the remaining methods as described in gradedExercise.md or on the website
  //      at http://flowerbird.de/scala4cl/exercises/graded_exercise/

}
