package com.ibm.s4cl.exercises.part06

import scala.annotation.tailrec

/**
  * Tail call optimization exercises
  */
object TailCallOptimization {
  
  // Exercise 1
  // Write a tail recursive list concatenation function.
  
  // Exercise 2
  // Write a tail-recursive function that evaluates expressions in reverse-Polish notation. Input
  // expression should be given as lists of RPTerm, see below. For more info on reverse-Polish
  // notation, see https://en.wikipedia.org/wiki/Reverse_Polish_notation
  
  sealed trait RPTerm
  sealed case class RPOperator(operatorChar: Char) extends RPTerm {
    def apply(r: Double, s: Double): Double = {
      operatorChar match {
        case '+' => r + s
        case '-' => r - s
        case '*' => r * s
        case '/' => r / s
      }
    }
  }
  sealed case class RPOperand(value: Double) extends RPTerm
  
  def evaluate(expression: List[RPTerm]): Double = {
    0
  }
  
  def main(args: Array[String]): Unit = {
    
  }
  
}
