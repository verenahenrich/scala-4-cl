package com.ibm.s4cl.exercises.part02

/**
  * An anagram is a word which is created from another word by rearranging its letters.
  * For example, since "phase" and "shape" consist of the same five letters -- just arranged in a
  * different order --, "phase" is an anagram of "shape" (and vice versa).
  */
object Anagram {

  val wordList = List("phase", "shape", "mode", "dome", "demo",
    "alerting", "altering", "integral", "relating", "triangle")

  /**
    * Extracts all anagrams for the specified word.
    *
    * @param word the word for which to extract anagrams
    * @return the list of extracted anagrams
    */
  def getAnagrams(word: String): List[String] = {

    // TODO: this method needs to be implemented

    // temporarily return an empty result until we have a proper implementation,
    // just to make the compiler happy
    List()
  }
}
