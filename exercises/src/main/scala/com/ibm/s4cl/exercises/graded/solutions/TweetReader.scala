package com.ibm.s4cl.exercises.graded.solutions

import scala.io.Source

class TweetReader(twitterData: Source) {

  // An (incomplete) set of delimiter characters
  val delimiter = "[ \t\n\r,.…?!\\-:;()\\[\\]'\"/*&$]+"

  // represents a list of all Tweets as read from file `Twitter_data.txt`
  val tweets: List[Tweet] =
    twitterData
      .getLines
      .map(line => line.split(",\t"))
      .map(line => line match {
        case Array(authorName, authorHandle, retweetCount, favoritesCount, text, postedTime) =>
          OriginalTweet(TweetAuthor(authorName, authorHandle),
            retweetCount.toInt,
            favoritesCount.toInt,
            text,
            postedTime,
            text.toLowerCase.split(delimiter).filter(_.startsWith("#")).toList) // hashtags in lower case
        case Array(authorName, authorHandle, text, postedTime) =>
          Retweet(TweetAuthor(authorName, authorHandle),
            text,
            postedTime,
            text.toLowerCase.split(delimiter).filter(_.startsWith("#")).toList) // hashtags in lower case
      })
      .toList
      .sortBy(tweet => tweet.getPostedTime)

  // originalTweets: represents a list of all original Tweets as read from file `Twitter_data.txt`
  // retweets: represents a list of all retweets as read from file `Twitter_data.txt`
  val (originalTweets, retweets): (List[OriginalTweet], List[Retweet]) =
    tweets
      .partition(tweet => tweet.isRetweet) match {
        case (retweets: List[Retweet], originalTweets: List[OriginalTweet]) => (originalTweets, retweets)
      }

  // represents the set of all Tweet's authors (authors from both original Tweets and retweets)
  val authors: Set[TweetAuthor] = tweets.map(_.getAuthor).toSet

  /**
    * Extracts all Tweets for the given TweetAuthor.
    * @param author the TweetAuthor for whom to extract all Tweets.
    * @return the list of Tweets for the given TweetAuthor.
    */
  def getTweetsForAuthor(author: TweetAuthor): List[Tweet] =
    tweets.filter(_.getAuthor == author)

  /**
    * Extracts all Tweets for all given TweetAuthors.
    * @param authors a Set of TweetAuthors for whom to extract all Tweets.
    * @return all Tweets for the given TweetAuthors.
    */
  def getTweetsForAuthors(authors: Set[TweetAuthor]): Set[Tweet] =
    authors.flatMap(author => getTweetsForAuthor(author))

  /**
    * Extracts all Tweets in which the given TweetAuthor is mentioned. An author is mentioned
    * in a Tweet means that the author's handle -- preceded by an @-symbol -- occurs in the Tweet's
    * text. For example, "This is a sample text mentioning @scala_lang." contains @scala_lang, i.e.,
    * it mentions author TweetAuthor("Scala", "scala_lang").
    * @param author the TweetAuthor for whom to extract all Tweets where he/she is mentioned.
    * @return the list of Tweets where the given TweetAuthor is mentioned.
    */
  def getTweetsWhereAuthorIsMentioned(author: TweetAuthor): List[Tweet] =
    tweets.filter(tweet =>
      tweet
        .getText
        .split(delimiter)
        .exists(token => token.contains("@" + author.handle))
    )

  /**
    * Extracts all Tweets in which at least one of the given TweetAuthors is mentioned. An author is
    * mentioned in a Tweet means that the author's handle -- preceded by an @-symbol -- occurs in the
    * Tweet's text. For example, "This is a sample text mentioning @scala_lang." contains @scala_lang,
    * i.e., it mentions author TweetAuthor("Scala", "scala_lang").
    * @param authors the TweetAuthors for whom to extract all Tweets where at least one of them is mentioned.
    * @return the list of Tweets where the given TweetAuthors are mentioned.
    */
  def getTweetsWhereAuthorsAreMentioned(authors: Set[TweetAuthor]): Set[Tweet] =
    authors.flatMap(author => getTweetsWhereAuthorIsMentioned(author))

  def getMentionedAuthorsInTweet(tweet: Tweet): Set[TweetAuthor] = {
    val authorHandles = authors.map(_.handle)
    tweet.getText
      .split(delimiter)
      .filter(token => token.startsWith("@"))
      .filter(token => authorHandles.contains(token.drop(1)))
      .flatMap(token => authors.filter(_.handle == token.drop(1)))
      .toSet
  }

  def getMentionedAuthorsInTweets(tweets: Set[Tweet]): Set[TweetAuthor] =
    tweets.flatMap(tweet => getMentionedAuthorsInTweet(tweet))

  def mostRetweeted(threshold: Int = 0): List[OriginalTweet] =
    originalTweets
      .filter(_.retweetCount >= threshold)
      .sortBy(tweet => -tweet.retweetCount)

  def mostOftenRetweetedAuthors(threshold: Int = 0): List[TweetAuthor] =
    mostRetweeted(threshold)
      .map(_.getAuthor)

  private def getHashtagFrequencies(): Map[String, Int] =
    tweets
      .flatMap(tweet => tweet.getHashtags)
      .groupBy(identity)
      .mapValues(_.length)

  def getHashtagFrequency(hashtag: String): Int =
    getHashtagFrequencies().getOrElse(hashtag.toLowerCase, 0)

  def hashtagSimilarity(tweet1: OriginalTweet, tweet2: OriginalTweet): Double = {
    val hashtags1 = tweet1.getHashtags
    val hashtags2 = tweet2.getHashtags
    val wordsInCommon = hashtags1.intersect(hashtags2)
    val allWords = (hashtags1 ++ hashtags2).distinct
    wordsInCommon.size / allWords.size.toDouble // normalized by total number of words in common
  }

  def findMostSimilarTweetsByHashtags(tweet: OriginalTweet): List[OriginalTweet] = {
    val similarities = originalTweets
      .filter(_ != tweet)
      .map(t => (t, hashtagSimilarity(t, tweet)))
      .toMap
    val maxSim = similarities.values.max
    similarities.filter(_._2 == maxSim).map(_._1).toList
  }

  def getRelatedAuthors(author: TweetAuthor, depth: Int): Set[TweetAuthor] = {
    def getRelatedAuthors(relatedAuthors: Set[TweetAuthor], currentDepth: Int): Set[TweetAuthor] = {
      if (currentDepth <= depth) {
        val tweetsForAuthors = getTweetsForAuthors(relatedAuthors)
        val mentionedAuthors = getMentionedAuthorsInTweets(tweetsForAuthors)
        val authorsBeingMentioned = getTweetsWhereAuthorsAreMentioned(relatedAuthors).map(_.getAuthor)
        getRelatedAuthors(relatedAuthors ++ mentionedAuthors ++ authorsBeingMentioned, currentDepth + 1)
      } else {
        relatedAuthors
      }
    }

    getRelatedAuthors(Set(author), 1)
  }

}
