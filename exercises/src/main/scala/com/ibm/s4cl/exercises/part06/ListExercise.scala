package com.ibm.s4cl.exercises.part06


object ListExercise {

  // Reverse a list using list concatenation
  def naiveReverse[T](in: List[T]): List[T] = {
    in match {
      case Nil => Nil
      case head::tail => naiveReverse(tail) ::: head::Nil
    }
  }
  
  //TODO: implement list reverse without list concatenation
  def reverse[T](in: List[T]): List[T] = {
    Nil // TODO: replace with proper implementation
  }
  
}
