package com.ibm.s4cl.exercises.part06

/**
  * A node in the dictionary tree.
  * TODO: implement
  */
class Node {
  
  // Encode the daughter nodes as an array of nodes. This is just a suggestion, you can define your
  // data structure differently if you like.
  var dtrs: Array[Node] = Array()
}

/**
  * A span of text. Use to return dictionary match results. The String.substring() function should
  * return the string corresponding to the dictionary match when used with the start and end
  * positions of the match.
  *
  * @param start Start of the match.
  * @param end End of the match.
  */
case class Span(start: Int, end: Int)

/**
  * A dictionary, implemented as a trie.
  */
class TrieDictionary {
  
  // Add a single word to the dictionary.
  // TODO: implement
  def addWord(word: String): TrieDictionary = {
    this
  }
  
  // Add a sequence of words to the dictionary
  // TODO: implement
  def addAll(words: Iterable[String]): TrieDictionary = {
    this
  }
  
  /**
    * Return all dictionary matches for the input text.
    *
    * @param text The text to match against.
    * @return The list of all matches for the text.
    */
  // TODO: implement
  def findAll(text: String): List[Span] = {
    Nil
  }
  
  // Check if an individual word is in the dictionary
  // TODO: implement
  def find(word: String): Boolean = {
    false
  }
  
}

/**
  * Companion object to TrieDictionary class.
  */
object TrieDictionary {
  
  // Create a new dictionary from a list of words.
  // TODO: implement
  def apply(words: String*): TrieDictionary = {
    null
  }

  // Copy the code from the recursion slides and adapt to this particular case.
  def binarySearch(ar: Array[Node], el: Char): Int = {
    0
  }

  // Create a string representation of a match in context.
  def printSpan(span: Span, text: String): String = {
    val contextSize = 20
    val prefixStart = Math.max(0, span.start - contextSize)
    val prefix = text.substring(prefixStart, span.start)
    val postfixEnd = Math.min(text.size, span.end + contextSize)
    val postfix = text.substring(span.end, postfixEnd)
    s"$prefix>>${text.substring(span.start, span.end)}<<$postfix"
  }
}
