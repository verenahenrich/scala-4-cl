package com.ibm.s4cl.exercises.part03.solutions

class NamedEntities(treebankFile: String) {

  // initialize an instance of PosLists, which reads file 'treebankFile' and creates a list of
  // tuples representing all word tokens with their POS tags; 'posLists.listOfWordTokensWithPos'
  // can/should be used to solve the current task
  val posLists = new PosLists(treebankFile)

  /**
    * Extracts all named entities (both single and multi token entities) from file 'treebankFile'.
    * This methods combines multi token named entities into one entry (by concatenating the tokens
    * with spaces in-between). Whether or not a named entity is represented by a single or by
    * multiple tokens is identified simply by their part-of-speech tags: If consecutive word tokens
    * have the same POS tag "NE", it is assumed they belong to the same named entity.
    *
    * For example, the output for the following list:
    *     List(("Peter", "NE"), ("likes", "V"), ("New", "NE"), ("York", "NE"))
    * is as follows:
    *     List("Peter", "New York")
    *
    * @return the list of named entities containing both single and multi token entities
    */
  def getNamedEntities(): List[String] = {

    // This is a recursive inner helper method to extract named entities as specified in the
    // description of the outer method 'getNamedEntities'
    def extractAndCombineNEs(wordTokensWithPos: List[(String, String)]): List[String] = {

      // if the input list is empty
      if (wordTokensWithPos == Nil) {
        // return the empty list
        Nil

      // if the input list is non-empty
      } else {
        // split the list as follows:
        //   - 'equalPos' represents the longest prefix sublist whose elements share the same POS
        //   - 'rest' represents the remainder of the list
        val (equalPos, rest) = wordTokensWithPos.span(_._2 == wordTokensWithPos.head._2)

        // if the tokens in the extracted prefix sublist are no NEs
        if (equalPos.head._2 != "NE") {
          // recursive call with the remainder of the list
          extractAndCombineNEs(rest)

        // if the extracted prefix sublist tokens are NEs
        } else {
          // add a list element representing the prefix sublist NE to the result AND continue
          // recursion with the remainder of the list
          convertNEEntryToString(equalPos) :: extractAndCombineNEs(rest)
        }
      }
    }

    // Extracts the first elements of the tuples from the input list and concatenates them with
    // spaces in-between. This is an inner helper method.
    def convertNEEntryToString(wordTokensWithPos: List[(String, String)]): String =
      wordTokensWithPos
        // extract the first elements of the tuples from the input list
        .map(_._1)
        // concatenate the list items with spaces in-between
        .mkString(" ")

    // call the inner helper method to extract all named entities
    extractAndCombineNEs(posLists.listOfWordTokensWithPos)
  }
}
