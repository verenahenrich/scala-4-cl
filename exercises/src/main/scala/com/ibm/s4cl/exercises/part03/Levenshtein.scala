package com.ibm.s4cl.exercises.part03

/**
  * The Levenshtein distance algorithm calculates the difference (distance) between two strings.
  * The lower the distance the more similar the two strings are. The distance between two strings
  * is determined by the minimum number of operations needed to transform one string into the other
  * using the operations of insertion, deletion, and substitution of a single character.
  */
object Levenshtein {

  def distance(s1: String, s2: String): Int = {

    // TODO: this method needs to be implemented

    -1
  }
}
