# Scala4CL: Graded Exercise on November 28, 2016

## Preparation

* Download the [zip file `graded_exercise_Your_Name.zip`](http://flowerbird.de/scala4cl/exercises/graded_exercise/graded_exercise_Your_Name.zip)
  and extract the project.
* Replace `Your_Name` in the project folder's name with your name, e.g., `graded_exercise_Verena_Henrich`.
* Open the project with IntelliJ: File -> Open... -> path to renamed project folder.


## Your Tasks

1. Take a look at file `Twitter_data.txt`, which contains a list of Tweets (one per line).
   The Tweets are a mixture of original Tweets and retweets. The format is as follows:
    * __Original Tweets__ consist of the following six fields, which are separated by `,<tab>`:
      `Author name,    author handle,  retweet count, favorites count,  text,   timestamp`
    * __Retweets__ consist of the following four fields, which are also separated by `,<tab>`:
      `Author name,    author handle,  text,   timestamp`
2. Take a look at the already provided classes `Tweet`, `TweetAuthor`, `OriginalTweet`, and `Retweet`.
3. Let's get started: Initialize fields `tweets`, `originalTweets`, `retweets`, and `authors`
   in class `TweetReader`. Do this by parsing file `Twitter_data.txt`, which is
   provided in the `twitterData: Source` parameter or the `TweetReader` constructor.
   Use pattern matching for deciding whether a line read from the file represents an
   original Tweet or a retweet.
    * __`tweets: List[Tweet]`__ should represent a list of all Tweets from file `Twitter_data.txt`.
      Store the Tweets chronologically, the oldest Tweet first, i.e., in ascending order by their timestamps.
      (Maybe you want to look at the [slides](https://bitbucket.org/verenahenrich/scala-4-cl/raw/master/slides/S4CL_02_functions_and_lists.pdf)
      again.)
    * __`originalTweets: List[OriginalTweet]`__ should represent all original Tweets as
      instances of class `OriginalTweet` as described above. The Tweets should be in chronological order.
    * __`retweets: List[Retweet]`__ should represent all retweets as
      instances of class `Retweet` as described above -- sorted chronologically.
    * __`authors: Set[TweetAuthor]`__ should represent a set of all Tweet's authors as
      instances of class `TweetAuthor`.
4. When you are done with the above points, the first four Scala unit tests in file
   `TweetReaderTest` should pass.
5. Implement the following four method stubs in class `TweetReader` according to
   their Scaladoc descriptions:
    * `getTweetsForAuthor(author: TweetAuthor): List[Tweet]`
    * `getTweetsForAuthors(authors: Set[TweetAuthor]): Set[Tweet]`
    * `getTweetsWhereAuthorIsMentioned(author: TweetAuthor): List[Tweet]`
    * `getTweetsWhereAuthorsAreMentioned(authors: Set[TweetAuthor]): Set[Tweet]`
6. When you have implemented these four methods, the next two Scala unit tests in
   `TweetReaderTest` should pass. If the first six tests pass, you have achieved 100% and are done
   with the exercise. You may continue with the optional bonus tasks or may just submit your work
   as described below.
   

## Bonus

* If you like to continue, create and implement the following methods. You might want
   to take a look at the corresponding unit tests in `TweetReaderTest` for what
   parameters and results are expected from these methods. The corresponding unit tests are
   commented out initially. Comment them in, implement the corresponding methods
   so that the tests compile, and make the tests pass. It is up to you to define
   further helping methods for the implementation of the following ten methods.
   
    * `getMentionedAuthorsInTweet`: takes a `Tweet` as an argument. Returns all `TweetAuthors`
      (as a `Set`) who are mentioned in the Tweet's text by a preceding `@`. For example,
      "This is a sample text mentioning @scala_lang." contains `@scala_lang`, which is called
      a handle. Your task is to extract those `@`-mentions which also occur in our previously
      constructed `authors` list -- in point (3) above.
    * `getMentionedAuthorsInTweets`: similar to `getMentionedAuthorsInTweet`, but
      takes a `Set` of `Tweets` as an argument and returns all `TweetAuthors`
      (again as a `Set`) mentioned in one of these Tweets.
    * `mostRetweeted`: returns a list of `OriginalTweets` sorted by how often
      a Tweet has been retweeted. It takes a `threshold` argument to return
      only those Tweets that have a retweet count at least of the given
      `threshold`. The default value of the `threshold` argument should be `0`.
      That is, if method `mostRetweeted` is called without arguments, it should
      return all original Tweets, which means in that case it returns the same elements
      as in the `originalTweets` list -- although in a different order.
    * `mostOftenRetweetedAuthors`: this method uses the same logic as described for
      method `mostRetweeted`; It returns a list of corresponding authors for those Tweets.
      This method also takes a `threshold` argument with a default value of `0`.
    * `getHashtagFrequency`: takes a hashtag as an argument and return
      the number of how often this hashtag occurs in the list of `tweets`.
    * `hashtagSimilarity`: takes two `Tweets` as arguments. Similarity is
      calculates as _the number of hashtags the two Tweets have in common_
      divided by _the total number of distinct hashtags for the two Tweets_.
    * `findMostSimilarTweetsByHashtags`: takes an `OriginalTweet` as an argument
      and returns the most similar `OriginalTweets` (there might be more than
      one most similar tweet, i.e., the return type is a list of `OriginalTweets`)
      according to the similarity measure implemented in method `hashtagSimilarity`.
    * Now you can commented in all tests in `TweetReaderTest` and they should pass.



## Submission

* Zip the project folder, which must include your full name, e.g., `graded_exercise_Verena_Henrich`.
* Send the zip file to [scala4cl@gmail.com](mailto:scala4cl@gmail.com).
* __Deadline: Monday, November 28, 2016, 6 p.m.__
