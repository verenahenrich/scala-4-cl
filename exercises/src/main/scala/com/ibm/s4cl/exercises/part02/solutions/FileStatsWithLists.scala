package com.ibm.s4cl.exercises.part02.solutions

import scala.io.Source

object FileStatsWithLists {

  // An (incomplete) set of delimiter characters.
  val delimiter = "[ \t\n\r,.?!\\-:;()\\[\\]'\"/*#&$]+"

  // The file to use for the exercise
  val fileName = "/austen-complete.txt"

  // The tokens read from the file
  val tokens: List[String] = extractTokensFromFile(fileName)

  // Create an array of unique, lower-cased tokens
  val normalizedTokens: List[String] = normalizeTokens(tokens)

  def main(args: Array[String]): Unit = {

    println(s"Number of tokens in file: ${tokens.length}") // expected: 792858
    println(s"Number of unique tokens: ${tokens.distinct.length}") // expected: 17563
    println(s"Number of unique normalized tokens: ${normalizedTokens.length}") // expected: 15397

    println(s"Number of 'Ladies' in the text: ${extractPerson("Lady").size}") // expected: 33
    println(s"'Ladies' in the text: ${extractPerson("Lady")}") // expected: Elliot, Russell, Mary, Wentworth, etc.
    println(s"Number of 'Misters' in the text: ${extractPerson("Mr").size}") // expected: 113
    println(s"'Misters' in the text: ${extractPerson("Mr")}") // expected: Elliot, Shepherd, Musgrove, Robinson, etc.

  }

  /**
    * Extract the list of tokens from the provided file 'fileName'. First, read in the lines of text
    * from the input file. Use the 'delimiter' regex to split the lines into tokens.
    *
    * @param fileName the name of the file to be read
    * @return the list of tokens read from the file
    */
  def extractTokensFromFile(fileName: String): List[String] = {
    Source
      .fromURL(getClass.getResource(fileName))
      .getLines()
      .flatMap(line => line.split(delimiter))
      .filter(token => !token.isEmpty)
      .toList
  }

  /**
    * Normalize the list of tokens, i.e., create a list of unique, lower-cased tokens.
    *
    * @param tokens the list of tokens to be normalized
    * @return a list of normalized tokens
    */
  def normalizeTokens(tokens: List[String]): List[String] = {
    tokens
      .map(token => token.toLowerCase)
      .distinct
  }

  /**
    * Extract the frequency of the given token, i.e., how often the token occurs in the text.
    *
    * @param token the token for which to extract the frequency
    * @return the frequency of the token
    */
  def extractFrequency(token: String): Int = {
    tokens.count(tok => tok == token)
  }

  /**
    * Extracts the names of all persons with the specified 'title'. For example, if we want to
    * extract persons with the given title "Lady" and the text contains
    *   "when Lady Susan left and Lady Bertram came",
    * then this method should extract List("Susan", "Bertram"). More specifically, this method
    * extracts the tokens that follow an occurrence of the provided 'title', with the restrictions
    * that the extracted tokens have to start with an uppercase first character and have to be at
    * least two characters in length.
    *
    * The implementation of this function should use a local function that extracts the relevant
    * tokens recursively.
    *
    * @param title the title for which to extract people
    * @return the list of names for the specified title
    */
  def extractPerson(title: String): List[String] = {

    /**
      * Local function to extract the tokens that follow an occurrence of the provided 'title'.
      *
      * @param tokens the remaining tokens to search for occurrences of 'title'
      * @return the list of tokens that follow an occurrence of 'title'
      */
    def extractPersonRecursively(tokens: List[String]): List[String] = {
      // ignore all tokens that do not match the provided 'title'
      val person = tokens.dropWhile(token => token != title)

      // base case: if no occurrence of 'title' has been found, i.e., if the remaining list is empty
      if (person.isEmpty) {
        // return the empty list
        Nil

      // recursive case: if 'title' has been found
      } else {
        // prepend the resulting list with the token following 'title' (which is 'person.tail.head')
        // and continue the recursion with the remaining sublist of tokens
        person.tail.head :: extractPersonRecursively(person.tail.tail)
      }
    }

    // initial call to the recursive method
    extractPersonRecursively(tokens)
      .distinct // consider distinct tokens only
      .filter(person => person.charAt(0).isUpper) // only consider names starting with an uppercase first character
      .filter(person => person.length > 1) // only consider names that consist of at least two characters
  }

}
