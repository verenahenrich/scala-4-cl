package com.ibm.s4cl.exercises.part02

/**
  * Simple noun compound splitter based solely on a list of simplex nouns.
  *
  * It splits compound words (written as one word, i.e., without spaces in-between) into sequences
  * of simplex nouns where the simplex nouns have to exist in the provided list of nouns.
  *
  * For some compound words several splits are possible. For example, the German compound
  * 'Druckerzeugnis' might be split into 'Druck+Erzeugnis' or 'Drucker+Zeugnis'. This compound
  * splitter does not intend to identify the 'correct' split or disambiguate between several
  * possible splits. It simply returns all identified splits.
  *
  * @param nounListFile file containing simplex nouns
  */
class CompoundSplitter(nounListFile: String) {

  // represents the list of simplex nouns as read from file 'nounListFile'
  private val simplexNouns: List[String] = List() // TODO: this variable needs to be initialized

  /**
    * Splits the given string 's' into all appropriate concatenations of simplex nouns.
    *
    * @param s the string to be split
    * @return a list of possible splits for 's', where each split represents a list of simplex nouns
    */
  def split(s: String): List[List[String]] = {

    // TODO: this method needs to be implemented

    // temporarily return an empty result until we have a proper implementation,
    // just to make the compiler happy
    List(List())

  }

  /**
    * Determines whether or not the given string is a compound. It returns true if there is at
    * least one possible split identified by the above split() method -- false otherwise.
    *
    * @param s the string to be checked
    * @return whether or not the string is a compound
    */
  def isCompound(s: String): Boolean = false // TODO: this method needs to be implemented
}
