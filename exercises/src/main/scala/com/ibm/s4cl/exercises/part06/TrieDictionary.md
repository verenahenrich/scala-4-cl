# Trie dictionary exercise

Implement a trie dictionary, that is, a dictionary that 
is organized as a prefix tree. Each node represents a certain
prefix, and for each possible character continuation, there is
another node (i.e., a subtree).

1. Think about the data that the node needs to contain. We put a
    suggestion for the basic data structure in the file. Implement
    the `addWord()` operation. If you keep the nodes in the array
    sorted, you can later use binary search on them.
    
2. To find matches, you need to traverse the input text and the
    tree in parallel. You can use the binarySearch function we
    discussed in class for fast search if you keep the nodes sorted.
    Implement the `find()` function. Activate the first test case.
    
3. To find matches in text, you need to return spans of the input.
    Implement the `findAll()` function. Activate the second test
    case. Make sure you have a correct implementation if one
    dictionary entry is a prefix of another one.
    
4. Activate the final test case. Does it complete in a reasonable
    amount of time? If not, why not?