package com.ibm.s4cl.exercises.part02.solutions

/**
  * Palindromes are strings that read the same forward and backward, such as the words "radar",
  * "madam", "eye", "kayak", and "testset".
  *
  * There are several ways to evaluate whether or not a given string is a palindrome. Below, you
  * find one solution to identifying palindromes. This solution (in method isPalindromeWithBuiltIn)
  * uses the built-in "reverse" method.
  *
  * Your task is to solve this task WITHOUT using this "reverse" method, but to solve this task
  * with basic list methods.
  */
object Palindrome {

  /**
    * Evaluates whether or not the provided string is a palindrome.
    * This implementation uses the built-in 'reverse' method.
    *
    * @param s the string to be evaluated
    * @return whether or not the string is a palindrome
    */
  def isPalindromeWithBuiltIn(s: String): Boolean =
    s == s.reverse

  /**
    * Evaluates whether or not the provided string is a palindrome.
    *
    * This implementation uses basic list methods and recursion. It is the preferred "functional"
    * approach as opposed to the non-recursive alternatives below.
    *
    * @param s the string to be evaluated
    * @return whether or not the string is a palindrome
    */
  def isPalindromeRecursively(s: String): Boolean =
    if (s.length <= 1) { // trivial case of zero or one character: always return 'true'
      true
    } else { // case where string has two or more characters
      // compare first and last characters
      // - if they differ: return false
      // - if they are equal: recursive call without first and last characters
      (s.head == s.last && isPalindromeRecursively(s.tail.init))
    }

  /**
    * Evaluates whether or not the provided string is a palindrome.
    *
    * This implementation uses list methods, but no recursion. Note that this implementation is not
    * the preferred solution, since it is far from a good functional style. We provide it simply for
    * comparison.
    *
    * @param s the string to be evaluated
    * @return whether or not the string is a palindrome
    */
  def isPalindromeNonRecursively(s: String): Boolean =
    if (s.length <= 1) { // if "s" has zero or one character, return 'true'
      true
    } else { // if "s" has two or more characters
      var index = 0 // counter variable
      // evaluate for all characters in "s" whether or not the character at position index+1 equals
      // the character at position length-index
      s.forall{
        c =>
          index = index + 1; // increase the counter
          c.equals(s(s.length-index)) // compare chars at positions "index" and "length-(index+1)"
      }
    }

  /**
    * Evaluates whether or not the provided string is a palindrome.
    *
    * This implementation uses a "for" expression, but no recursion. Note that this implementation
    * is not the preferred solution, since it is far from a good functional style. We provide it
    * simply for comparison.
    *
    * @param s the string to be evaluated
    * @return whether or not the string is a palindrome
    */
  def isPalindromeNonRecursivelyWithFor(s: String): Boolean =
    if (s.length <= 1) { // if "s" has zero or one character, return 'true'
      true
    } else { // if "s" has two or more characters
      var isPalindrome = true
      // use a "for" expression to iterate through the first half if "s"
      for (index <- 0 to s.length/2) {
        // compare characters at positions "index" and "length-1-index"
        if (!s(index).equals(s(s.length-1-index))) {
          // if there is at least one mismatch, set the result to 'false'
          isPalindrome = false
        }
      }

      isPalindrome
    }
}
