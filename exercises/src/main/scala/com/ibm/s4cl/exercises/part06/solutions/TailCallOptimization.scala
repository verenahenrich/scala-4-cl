package com.ibm.s4cl.exercises.part06.solutions

import scala.annotation.tailrec

/**
  * Tail call optimization exercises
  */
object TailCallOptimization {

  // Exercise 1
  // Write a tail recursive list concatenation function.

  def concat[T](list1: List[T], list2: List[T]): List[T] = {
    @tailrec
    def concatTailRec[T](remainingList: List[T], acc: List[T]): List[T] = remainingList match {
        case Nil => acc
        case h :: t => concatTailRec(t, h :: acc)
      }

    concatTailRec(list1.reverse, list2)
  }

  // Exercise 2
  // Write a tail-recursive function that evaluates expressions in reverse-Polish notation. Input
  // expression should be given as lists of RPTerm, see below. For more info on reverse-Polish
  // notation, see https://en.wikipedia.org/wiki/Reverse_Polish_notation
  
  sealed trait RPTerm
  sealed case class RPOperator(operatorChar: Char) extends RPTerm {
    def apply(r: Double, s: Double): Double = {
      operatorChar match {
        case '+' => r + s
        case '-' => r - s
        case '*' => r * s
        case '/' => r / s
      }
    }
  }
  sealed case class RPOperand(value: Double) extends RPTerm

  def evaluate(expression: List[RPTerm]): Double = {
    @tailrec
    def evaluateTailRec(expression: List[RPTerm], stack: List[Double]): Double =
      (expression, stack) match {
        case (Nil, stackHead :: _) => stackHead
        case (RPOperand(value) :: exprTail, _) => evaluateTailRec(exprTail, value :: stack)
        case ((op: RPOperator) :: exprTail, operand2 :: operand1 :: remainingStack) =>
          evaluateTailRec(exprTail, op(operand1, operand2) :: remainingStack)
      }

    evaluateTailRec(expression, Nil)
  }

  def main(args: Array[String]): Unit = {

    val list1 = List("a", "b", "c")
    val list2 = List("d", "e", "f")
    println(concat(list1, list2))

    // 5 4 +
    val expr1 = List(RPOperand(5), RPOperand(4), RPOperator('+'))
    println(evaluate(expr1)) // expected: 9

    // 5 1 2 + 4 × + 3 −
    val expr2 = List(RPOperand(5), RPOperand(1), RPOperand(2), RPOperator('+'), RPOperand(4),
      RPOperator('*'), RPOperator('+'), RPOperand(3), RPOperator('-'))
    println(evaluate(expr2)) // expected: 14

    // 23 2 * 4 -
    val expr3 = List(RPOperand(23), RPOperand(2), RPOperator('*'), RPOperand(4), RPOperator('-'))
    println(evaluate(expr3)) // expected: 42
  }
  
}
