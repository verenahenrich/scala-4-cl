package com.ibm.s4cl.exercises.part02

/**
  * Palindromes are strings that read the same forward and backward, such as the words "radar",
  * "madam", "eye", "kayak", and "testset".
  *
  * There are several ways to evaluate whether or not a given string is a palindrome. Below, you
  * find one solution to identifying palindromes. This solution (in method isPalindromeWithBuiltIn)
  * uses the built-in "reverse" method.
  *
  * Your task is to solve this task WITHOUT using this "reverse" method, but to solve this task
  * with basic list methods.
  */
object Palindrome {

  /**
    * Evaluates whether or not the provided string is a palindrome.
    * This implementation uses the built-in 'reverse' method.
    *
    * @param s the string to be evaluated
    * @return whether or not the string is a palindrome
    */
  def isPalindromeWithBuiltIn(s: String): Boolean =
    s == s.reverse

  /**
    * Evaluates whether or not the provided string is a palindrome.
    *
    * @param s the string to be evaluated
    * @return whether or not the string is a palindrome
    */
  def isPalindrome(s: String): Boolean =
    // TODO: this method needs to be implemented
    false

}
