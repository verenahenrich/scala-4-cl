package com.ibm.s4cl.exercises.part02.solutions

import scala.io.Source

/**
  * Simple noun compound splitter based solely on a list of simplex nouns.
  *
  * It splits compound words (written as one word, i.e., without spaces in-between) into sequences
  * of simplex nouns where the simplex nouns have to exist in the provided list of nouns.
  *
  * For some compound words several splits are possible. For example, the German compound
  * 'Druckerzeugnis' might be split into 'Druck+Erzeugnis' or 'Drucker+Zeugnis'. This compound
  * splitter does not intend to identify the 'correct' split or disambiguate between several
  * possible splits. It simply returns all identified splits.
  *
  * The splitting does not consider all morphological processes that might be involved in
  * compounding -- for several languages. For example, it ignores intervening linking elements
  * such as in the German compound 'Birne+n+baum' and it ignores the elision of word-final
  * characters in the modifier constituent of German compounds such as in 'Schulklasse', where
  * the final 'e' in the simplex noun 'Schule' needs to be dropped.
  *
  * @param nounListFile file containing simplex nouns
  */
class CompoundSplitter(nounListFile: String) {

  // represents the list of simplex nouns as read from file 'nounListFile'
  private val simplexNouns: List[String] =
    Source.fromURL(getClass.getResource(nounListFile)).getLines.toList.sorted

  /**
    * Splits the given string 's' into all appropriate concatenations of simplex nouns.
    *
    * @param s the string to be split
    * @return a list of possible splits for 's', where each split represents a list of simplex nouns
    */
  def split(s: String): List[List[String]] = {

    /**
      * Recursive splitting method.
      *
      * @param s the remaining string to be split
      * @param acc the so far accumulated result
      * @return a list of possible splits for 's', where each split represents a list of simplex
      *         nouns
      */
    def splitRecursively(s: String, acc: List[String]): List[List[String]] = {
      // if 's' is empty, the end of the string has been reached recursively and the recursion stops
      if (s.isEmpty) {
        if (acc.size <= 1) {
          // if the accumulated result list contains none or one element this means that no
          // composition of at least two simplex nouns has been found: an empty list is returned
          // in this case
          Nil
        } else {
          // if there are at least two elements in the accumulated result list, this means that the
          // compound has been successfully split into at least two simplex nouns: the result is
          // wrapped into a list and returned
          List(acc)
        }
      } else { // if 's' is not empty (recursive case)
        // filter the list of simplex nouns for those that represent a starting substring of 's'
        simplexNouns
          .filter(simplexNoun => s.startsWith(simplexNoun.toLowerCase))
          // for each such filtered noun
          .flatMap(simplexNoun => {
            // get the substring of 's' where the simplex noun has been removed from the beginning
            val remainingS = s.drop(simplexNoun.length)
            // add the simplex noun to the accumulated result
            val newAcc = acc :+ simplexNoun
            // recursive call to splitRecursively() with the remaining substring
            splitRecursively(remainingS, newAcc)
          })
      }
    }

    // initial call to the recursive splitting method
    splitRecursively(s.toLowerCase, Nil)
  }

  /**
    * Determines whether or not the given string is a compound. It returns true if there is at
    * least one possible split identified by the above split() method -- false otherwise.
    *
    * @param s the string to be checked
    * @return whether or not the string is a compound
    */
  def isCompound(s: String): Boolean = !split(s).isEmpty
}
