package com.ibm.s4cl.exercises.part01.solutions

import java.util
import java.util.regex.Pattern

import scala.collection.mutable
import scala.io.Source
import scala.util.Sorting

object FileStats {

  // An (incomplete) set of delimiter characters.
  val delimiter = "[ \t\n\r,.?!\\-:;()\\[\\]'\"/*#&$]+"

  // The file to use for the exercise
  val fileName = "exercises/src/main/resources/austen-complete.txt"

  def main(args: Array[String]): Unit = {

    // Read in the lines of text from the input file. Print the number of lines in the file.
    val lines: Seq[String] = Source
      .fromFile(fileName)
      .getLines()
      .toSeq
    println(s"Number of lines in file: ${lines.length}")

    // Use the 'delimiter' regex to split the lines into tokens. Print the number of tokens in the file.
    val tokens: Seq[String] = for (
      line <- lines;
      token <- line.split(delimiter) if token.length > 0
    ) yield token
    println(s"Number of tokens in file: ${tokens.length}")

    // Print out the number of unique tokens
    println(s"Number of unique tokens: ${tokens.distinct.length}")

    // Create an array of unique, lower-cased tokens. Print the size of the array.
    val normalizedTokens = (for (token <- tokens) yield token.toLowerCase)
      .distinct
      .toArray

    println(s"Number of unique normalized tokens: ${normalizedTokens.length}")

    // Count the number of occurrences for each (lower-cased) token in the text. Use an array of integers that is
    // parallel to the normalizedTokens array to do this. Hint: sort the normalizedTokens array first, then use the
    // 'findString' method below to access token positions. You don't have to use sorting, but it will be very slow
    // otherwise.
    Sorting.quickSort(normalizedTokens)

    val counts: Array[Int] = Array.fill[Int](normalizedTokens.length)(0)
    for (token <- tokens) {
      val position = findString(normalizedTokens, token.toLowerCase())
      val count = counts(position)
      counts(position) = count + 1
    }

    // Print out the counts for the tokens 'the', 'pride', 'prejudice' and 'melrose'.
    val toPrint = Seq("the", "pride", "prejudice", "melrose")
    for (token <- toPrint) {
      val pos = findString(normalizedTokens, token)
      val count = if (pos < 0) 0 else counts(pos)
      println(s"Token '$token' occurred $count times.")
    }

    // Use the Java regular expression facility to determine the longest contiguous sequence of delimiter characters
    // in the file.
    val text = Source
      .fromFile(fileName)
      .getLines()
      .mkString
    val spacePattern = Pattern.compile(delimiter)
    val spaceMatcher = spacePattern.matcher(text)
    var max = 0
    while (spaceMatcher.find()) {
      val length = spaceMatcher.group().length
      if (length > max) max = length
    }
    println(s"The longest sequence of delimiter characters is $max long.")

    // Optional: look up the documentation for Scala maps. Redo the token counting exercise with a map from strings to
    // integers, instead of the parallel array.
    println("Optional: repeating the token counting with a map.")
    // Use a mutable map
    val countMap = mutable.Map[String, Int]()
    for (token <- tokens) {
      val normToken = token.toLowerCase
      val countOption = countMap.get(normToken)
      if (countOption.isDefined) {
        val count = countOption.get
        countMap.put(normToken, count + 1)
      } else {
        countMap.put(normToken, 1)
      }
    }
    for (token <- toPrint) {
      val count = countMap.get(token).getOrElse(0)
      println(s"Token '$token' occurred $count times.")
    }

  }

  // Find the position of a string in a sequence of strings, using Arrays.binarySearch() from the Java standard library.
  // Do not worry about the details here, but look up what Arrays.binarySearch() returns.
  private def findString(stringSeq: Seq[String], s: String): Int = {
    val ar = stringSeq.toArray.asInstanceOf[Array[AnyRef]]
    util.Arrays.binarySearch(ar, s)
  }

}
