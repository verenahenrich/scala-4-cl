package com.ibm.s4cl.exercises.graded.solutions

import OpenNLP.{getPosTags, getSentences, getTokens}

import scala.annotation.tailrec
import scala.io.Source

case class SentenceWithTokens(sentence: Span, tokens: List[PosToken])

object OpenNLPExercises {

  val text = {
    val is = getClass.getResourceAsStream("/pnp.txt")
    Source
      .fromInputStream(is)
      .mkString
  }

  // Exercise1: compute sentences, tokens and POS tags using the OpenNLP APIs.
  val sentences: List[SentenceWithTokens] = getTokenizedSentences()

  def getTokenizedSentences(): List[SentenceWithTokens] = {
    
    def getPosTokensForSentence(sent: Span): List[PosToken] = {
      val sentenceText = sent.getCoveredText(text)
      val tokens = getTokens(sentenceText)
      val posTags = getPosTags(sentenceText, tokens)
      tokens
        .zip(posTags)
        .map({case (token, pos) => PosToken(token, pos)})
        .map(pt => PosToken(Span(pt.span.start + sent.start, pt.span.end + sent.start), pt.pos))
    }
    
    getSentences(text)
      .map(s => SentenceWithTokens(s, getPosTokensForSentence(s)))
  }

  // Get token for span. (helping method)
  def getTokenAsString(token: PosToken): String = token.span.getCoveredText(text)

  // Get a list of tokens with their POS tags for the entire text. (helping method)
  def getTokensWithPos(): List[(String, String)] = {
    sentences
      .flatMap(s => s.tokens.map(token => (getTokenAsString(token), token.pos)))
  }

  // Exercise2: return the list of "sentence span"/"number of token" pairs, in ascending order by
  // number of tokens.
  def getSentencesByTokenCount(): List[(Span, Int)] = {
    sentences
      .map(s => (s.sentence, s.tokens.size))
      .sortBy(_._2)
  }
  
  // Exercise: Get Noun-Noun sequences for the entire text.
  def getNounNounCompounds2(): List[(Span, Span)] = {
    
    @tailrec
    def findNNs(ptList: List[PosToken], nnList: List[(Span, Span)], prev: Option[Span]): List[(Span, Span)] = {
      ptList match {
        case Nil => nnList
        case PosToken(span, pos)::tail => {
          val candidateSpan = if (pos == "NN") Some(span) else None
          prev match {
            case None => findNNs(tail, nnList, candidateSpan)
            case Some(prevSpan) => {
              val resList = if (pos == "NN") (prevSpan, span)::nnList else nnList
              findNNs(tail, resList, candidateSpan)
            } 
          }
        }
      }
    } 
    
    sentences
      .flatMap(s => {
        findNNs(s.tokens, Nil, None).reverse
      })
  }

  // Exercise: Get Noun-Noun sequences for the entire text.
  def getNounNounCompounds(): List[(Span, Span)] = {
    sentences
      .map(sentence => sentence.tokens
        .sliding(2)
        .filter(pair => pair.head.pos == "NN" && pair(1).pos == "NN")
        .map(list => (list.head.span, list(1).span))
      )
      .flatten
  }

  // Exercise: Extract those tokens that are tagged with different POS tags throughout the text;
  // This method should return a map where the keys are the tokens (as strings, not regarding
  // concrete token spans anymore) and the values are corresponding sets of POS tags occurring
  // anywhere in the text for the token; The resulting map should only contain tokens which occur
  // at least for two different POS.
  def getTokensWithMultiplePos(): Map[String, Set[String]] = {
    getTokensWithPos()
      .groupBy({case (token, pos) => token})
      .mapValues(tokenPosTuples => tokenPosTuples.map({case (token, pos) => pos}).toSet)
      .filter({case (token, occurringPos) => occurringPos.size > 1})
  }

  // Exercise: Extract POS trigrams (only within sentences) with corresponding token trigrams;
  // This method should return a map where the keys are three POS tags occurring as a sequence
  // anywhere in the text (sequences only within sentence boundaries, i.e., filter out sentences
  // that consist of less than three tokens) and the values are corresponding sets of token trigrams.
  def getPosTrigramsWithCorrespondingTokens(): Map[(String, String, String), Set[(String, String, String)]] = {

    val trigrams: List[(PosToken, PosToken, PosToken)] = sentences
      .filter(sentence => sentence.tokens.size > 2)
      .map(sentence => sentence.tokens.sliding(3))
      .flatten
      .map(trigram => (trigram.head, trigram(1), trigram(2)))

    trigrams
      .groupBy({case (posToken1, posToken2, posToken3) => (posToken1.pos, posToken2.pos, posToken3.pos)})
      .mapValues(trigrams =>
        trigrams.map({case (posToken1, posToken2, posToken3) =>
          (getTokenAsString(posToken1), getTokenAsString(posToken2), getTokenAsString(posToken3))}).toSet)
  }

}
