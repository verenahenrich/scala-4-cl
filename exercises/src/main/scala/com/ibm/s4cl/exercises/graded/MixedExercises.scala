package com.ibm.s4cl.exercises.graded

import scala.annotation.tailrec

object MixedExercises {

  // Exercise 1: rewrite the following recursive 'listSize' function in a tail-recursive way;
  //             make sure your code contains the '@tailrec' annotation
  // Comment in the corresponding test case when you're done
  def listSize(list: List[_]): Int = list match {
      case Nil => 0
      case _::tail => 1 + listSize(tail)
    }
  

  // Exercise 2: write a higher-order function argswitch that switches the arguments of any
  // two-argument function. That is, for any appropriate values a and b, f(a,b) = argswitch(f)(b, a).
  // Comment in the corresponding test case when you're done

  
  
}
