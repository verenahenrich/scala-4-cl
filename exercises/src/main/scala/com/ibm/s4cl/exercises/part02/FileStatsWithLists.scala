package com.ibm.s4cl.exercises.part02

import scala.io.Source

// TODO to solve this exercise try using the list methods that we discussed during the lecture
//      whenever possible (and at the same time try not to use any kind of loops)

object FileStatsWithLists {

  // An (incomplete) set of delimiter characters.
  val delimiter = "[ \t\n\r,.?!\\-:;()\\[\\]'\"/*#&$]+"

  // The file to use for the exercise
  val fileName = "/austen-complete.txt"

  // The tokens read from the file
  // TODO comment in the following line and implement the corresponding method
//  val tokens: List[String] = extractTokensFromFile(fileName)

  // Create an array of unique, lower-cased tokens
  // TODO comment in the following and implement the corresponding method
//  val normalizedTokens: List[String] = normalizeTokens(tokens)

  def main(args: Array[String]): Unit = {

    // TODO comment in the following lines and implement the corresponding methods
//    println(s"Number of tokens in file: ${tokens.length}") // expected: 792858
//    println(s"Number of unique tokens: ${tokens.distinct.length}") // expected: 17563
//    println(s"Number of unique normalized tokens: ${normalizedTokens.length}") // expected: 15397
//
//    println(s"Number of 'Ladies' in the text: ${extractPerson("Lady").size}") // expected: 33
//    println(s"'Ladies' in the text: ${extractPerson("Lady")}") // expected: Elliot, Russell, Mary, Wentworth, etc.
//    println(s"Number of 'Misters' in the text: ${extractPerson("Mr").size}") // expected: 113
//    println(s"'Misters' in the text: ${extractPerson("Mr")}") // expected: Elliot, Shepherd, Musgrove, Robinson, etc.

    // TODO instead of printing the calculated results to the standard output, create unit tests
    //      that test at least for all of the above method calls; put your unit test file into
    //      "exercises/src/test/scala/com/ibm/s4cl/exercises/part02", where you can also find
    //      example unit tests

  }

//  /**
//    * Extract the list of tokens from the provided file 'fileName'. First, read in the lines of text
//    * from the input file. Use the 'delimiter' regex to split the lines into tokens.
//    *
//    * @param fileName the name of the file to be read
//    * @return the list of tokens read from the file
//    */
  // TODO implement this method

//  /**
//    * Normalize the list of tokens, i.e., create a list of unique, lower-cased tokens.
//    *
//    * @param tokens the list of tokens to be normalized
//    * @return a list of normalized tokens
//    */
  // TODO implement this method

//  /**
//    * Extract the frequency of the given token, i.e., how often the token occurs in the text.
//    *
//    * @param token the token for which to extract the frequency
//    * @return the frequency of the token
//    */
  // TODO implement this method

//  /**
//    * Extracts the names of all persons with the specified 'title'. For example, if we want to
//    * extract persons with the given title "Lady" and the text contains
//    *   "when Lady Susan left and Lady Bertram came",
//    * then this method should extract List("Susan", "Bertram"). More specifically, this method
//    * extracts the tokens that follow an occurrence of the provided 'title', with the restrictions
//    * that the extracted tokens have to start with an uppercase first character and have to be at
//    * least two characters in length.
//    *
//    * The implementation of this function should use a local function that extracts the relevant
//    * tokens recursively.
//    *
//    * @param title the title for which to extract people
//    * @return the list of names for the specified title
//    */
  // TODO implement this method

}
