package com.ibm.s4cl.exercises.graded

import opennlp.tools.postag.{POSModel, POSTaggerME}
import opennlp.tools.sentdetect.{SentenceDetectorME, SentenceModel}
import opennlp.tools.tokenize.{TokenizerME, TokenizerModel}

import scala.io.Source

/**
  * Yet another span class. Extend as needed.
  *
  * @param start
  * @param end
  */
case class Span(start: Int, end: Int) {
  def getCoveredText(text: String) = text.substring(start, end)
}

/**
  * A token span together with its part of speech.
  *
  * @param span
  * @param pos
  */
case class PosToken(span: Span, pos: String)

/**
  * Utility functions to call OpenNLP from Scala.
  */
object OpenNLP {

  val sentenceSegmenter = {
    val is = getClass.getResourceAsStream("/opennlp/en-sent.bin")
    new SentenceDetectorME(new SentenceModel(is))
  }
  
  val tokenizer = {
    val is = getClass.getResourceAsStream("/opennlp/en-token.bin")
    new TokenizerME(new TokenizerModel(is))
  }
  
  val posTagger = {
    val is = getClass.getResourceAsStream("/opennlp/en-pos-maxent.bin")
    new POSTaggerME(new POSModel(is))
  }

  /**
    * Convert an OpenNLP span into one of our own.
    *
    * @param openNlpSpan An OpenNLP span.
    * @return The corresponding Scala span.
    */
  private def openNlpSpanToScalaSpan(openNlpSpan: opennlp.tools.util.Span) =
    Span(openNlpSpan.getStart, openNlpSpan.getEnd)

  /**
    * Split a text into a list of sentences.
    *
    * @param text The input text.
    * @return A list of spans representing the sentences in the text.
    */
  def getSentences(text: String): List[Span] = {
    val sentenceArray = sentenceSegmenter.sentPosDetect(text)
    sentenceArray
      .map(openNlpSpanToScalaSpan)
      .toList
  }

  /**
    * Do POS tagging for one sentence.
    *
    * @param text The text that the token spans are relative to.
    * @param tokens A list of spans that represent the tokens of a sentence.
    * @return A list of POS tags. This list has the same length as the list of input tokens.
    */
  def getPosTags(text: String, tokens: List[Span]): List[String] = {
    val tokenArray = tokens
      .map(_.getCoveredText(text))
      .toArray
    posTagger
      .tag(tokenArray)
      .toList
  }

  /**
    * Tokenize text.
    *
    * @param text The input text.
    * @return A list of spans, one for each token.
    */
  def getTokens(text: String): List[Span] = {
    val tokenArray = tokenizer.tokenizePos(text)
    tokenArray
      .map(openNlpSpanToScalaSpan)
      .toList
  }
  
  def main(args: Array[String]): Unit = {
    val dataIS = getClass.getResourceAsStream("/pnp.txt")
    val text = Source
      .fromInputStream(dataIS)
      .mkString
    val sentences = getSentences(text)
    sentences
      .take(10)
      .foreach(s => {
        val sText = s.getCoveredText(text)
        val tokens = getTokens(sText)
        val posTags = getPosTags(sText, tokens)
        val posTokens = tokens.zip(posTags)
        println(sText)
        posTokens
          .foreach({ case (span: Span, pos: String) =>
            println(s"${span.getCoveredText(sText)}, $pos")})
      })
  }
}
