package com.ibm.s4cl.exercises.part04

object ProgramArguments {

  val validArguments = "Valid arguments: " +
    "-h/--help, " +
    "--version, " +
    "-l=[language]/--lang=[language], " +
    "-i=[filename]/--input=[filename], " +
    "-p=[num]/--pages=[num]"

  val version = "Version: 1.0"

  def parseArgument(argument: (String, Any)): String =
    // TODO: this function should be implemented by using pattern matching;
    //       for some input you should call the helper functions defined below
    ""

  def setLanguage(language: String): String =
    // TODO: this function should be implemented by using pattern matching;
    ""

  def setInputFile(inputFile: String): String =
    // TODO: this function needs to be implemented
    ""

  def setNumPages(numPages: Int): String =
    // TODO: this function should be implemented by using pattern matching;
    ""

}
