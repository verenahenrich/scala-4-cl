package com.ibm.s4cl.exercises.part01

import java.util
import java.util.regex.Pattern

import scala.collection.mutable
import scala.io.Source
import scala.util.Sorting

object FileStats {

  // An (incomplete) set of delimiter characters.
  val delimiter = "[ \t\n\r,.?!\\-:;()\\[\\]'\"/*#&$]+"

  // The file to use for the exercise
  val fileName = "exercises/src/main/resources/austen-complete.txt"

  def main(args: Array[String]): Unit = {

    // Read in the lines of text from the input file. Print the number of lines in the file.
    // val lines: Seq[String] = ...
    // println(s"Number of lines in file: ${lines.length}")

    // Use the 'delimiter' regex to split the lines into tokens. Print the number of tokens in the file.
    // val tokens: Seq[String] = ...

    // Print out the number of unique tokens

    // Create an array of unique, lower-cased tokens. Print the size of the array.
    // val normalizedTokens = ...
    // println(s"Number of unique normalized tokens: ${normalizedTokens.length}")

    // Count the number of occurrences for each (lower-cased) token in the text. Use an array of integers that is
    // parallel to the normalizedTokens array to do this. Hint: sort the normalizedTokens array first, then use the
    // 'findString' method below to access token positions. You don't have to use sorting, but it will be very slow
    // otherwise.

    // Print out the counts for the tokens 'the', 'pride', 'prejudice' and 'melrose'.

    // Use the Java regular expression facility to determine the longest contiguous sequence of delimiter characters
    // in the file.
    // println(s"The longest sequence of delimiter characters is $max long.")

    // Optional: look up the documentation for Scala maps. Redo the token counting exercise with a map from strings to
    // integers, instead of the parallel array.
    // println("Optional: repeating the token counting with a map.")
    // Use a mutable map
    val countMap = mutable.Map[String, Int]()
    // Print out the counts for the tokens 'the', 'pride', 'prejudice' and 'melrose' again, making sure you get the same
    // values as above.
  }

  // Find the position of a string in a sequence of strings, using Arrays.binarySearch() from the Java standard library.
  // Do not worry about the details here, but look up what Arrays.binarySearch() returns.
  private def findString(stringSeq: Seq[String], s: String): Int = {
    val ar = stringSeq.toArray.asInstanceOf[Array[AnyRef]]
    util.Arrays.binarySearch(ar, s)
  }

}
