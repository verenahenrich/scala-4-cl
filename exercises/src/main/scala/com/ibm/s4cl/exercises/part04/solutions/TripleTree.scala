package com.ibm.s4cl.exercises.part04.solutions

/**
  * Represents trees recursively as triples. Leaves are simple strings. The triple elements in the
  * middle represent the nodes. The left and right triple elements represent the left and right
  * subtrees.
  *
  * For example:
  *   ("history", "NP", ("of", "PP", "Scala"))
  *
  * Represents the tree:
  *
  *           NP
  *          /  \
  *   history   PP
  *            /  \
  *          of   Scala
  */
object TripleTree {

  /**
    * Converts the given 'triple tree' into a String. Leaves and branches are surrounded by
    * parentheses. Nodes are surrounded by spaces.
    *
    * For example, representing ("history", "NP", ("of", "PP", "Scala")) as a String:
    * ((history) NP ((of) PP (Scala)))
    *
    * @param tree the 'triple tree' to be represented as a String
    * @return the string representation of the given 'triple tree'
    */
  def tripleTreeToString(tree: Any): String = tree match {
    case leaf: String => s"($leaf)"
    case (left, innerNode: String, right) =>
      s"(${tripleTreeToString(left)} $innerNode ${tripleTreeToString(right)})"
    case _ => "not allowed"
  }

  /**
    * Extracts all phrases of the specified type. The result is a list containing the String
    * representations of the extracted phrases. If there exist nested phrases of the specified type
    * only the most comprehensive phrase is extracted. Since the string representation of these
    * comprehensive phrases contain the nested phrases, the nested phrases are not listed
    * separately. If there are no relevant phrases, an empty list is returned.
    *
    * @param tree the 'triple tree' from which to extract phrases
    * @param phraseType the phrase type to be extracted
    * @return the list of extracted phrases
    */
  def extractPhrasesFromTripleTree(tree: Any, phraseType: String): List[String] = tree match {
    case (left, `phraseType`, right) => List(tripleTreeToString(tree))
    case (left, _: String, right) =>
      extractPhrasesFromTripleTree(left, phraseType) ::: extractPhrasesFromTripleTree(right, phraseType)
    case _ => Nil
  }

}
