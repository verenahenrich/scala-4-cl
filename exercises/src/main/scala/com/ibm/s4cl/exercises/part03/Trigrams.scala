package com.ibm.s4cl.exercises.part03

import scala.io.Source

/**
  * Trigrams are contiguous sequences of three word tokens in a given text.
  *
  * @param textFile text file to extract trigrams from
  */
class Trigrams(textFile: String) {

  // An (incomplete) set of delimiter characters.
  val delimiter = "[ \t\n\r,.?!\\-:;()\\[\\]'\"/*#&$]+"

  // a list of triples representing all trigram tokens (in lower case) -- as read from file 'textFile'
  val trigrams: List[(String, String, String)] = List() // TODO: this variable needs to be initialized

  /**
    * Predicts a plausible next word for a given bigram (word1, word2). The returned word is one of
    * the 'next' words with the highest frequency, as calculated from the list of trigrams extracted
    * from file 'textFile'.
    *
    * @param word1 the first word of the specified bigram
    * @param word2 the second word of the specified bigram
    * @return one of the most plausible 'next' words according to the calculated trigram frequencies
    */
  def predictNextWord(word1: String, word2: String): Option[String] = {

    // TODO: this method needs to be implemented

    None
  }
}
