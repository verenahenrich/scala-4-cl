package com.ibm.s4cl.exercises.part02.solutions

/**
  * An anagram is a word which is created from another word by rearranging its letters.
  * For example, since "phase" and "shape" consist of the same five letters -- just arranged in a
  * different order --, "phase" is an anagram of "shape" (and vice versa).
  */
object Anagram {

  val wordList = List("phase", "shape", "mode", "dome", "demo",
    "alerting", "altering", "integral", "relating", "triangle")

  /**
    * Extracts all anagrams for the specified word.
    *
    * @param word the word for which to extract anagrams
    * @return the list of extracted anagrams
    */
  def getAnagrams(word: String): List[String] =
    // Filter the "wordList"
    //   - Exclude the input word
    //   - Keep all words with the same characters (by comparing "sorted" words with each other)
    wordList.filter(w => !w.equals(word) && w.sorted.equals(word.sorted))
}
