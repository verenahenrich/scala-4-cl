package com.ibm.s4cl.exercises.part03.solutions

import scala.math.min

/**
  * The Levenshtein distance algorithm calculates the difference (distance) between two strings.
  * The lower the distance the more similar the two strings are. The distance between two strings
  * is determined by the minimum number of operations needed to transform one string into the other
  * using the operations of insertion, deletion, and substitution of a single character.
  */
object Levenshtein {

  def distance(s1: String, s2: String): Int = {

    // calculates the minimum of three integers
    def minimum(i1: Int, i2: Int, i3: Int) = min(min(i1, i2), i3)

    // initialize two-dimensional Levenshtein table
    val table = Array.ofDim[Int](s1.length + 1, s2.length + 1)

    // fill first column with 0, 1, 2, ...
    for (row <- 0 to s1.length)
      table(row)(0) = row

    // fill first row with 0, 1, 2, ...
    for (column <- 0 to s2.length)
      table(0)(column) = column

    // fill the rest of the table according to the Levenshtein algorithm
    for (row <- 1 to s1.length) {
      for (column <- 1 to s2.length) {

        val above = table(row - 1)(column)         // value in the cell above
        val left = table(row)(column - 1)          // value in the cell to the left
        val leftAbove = table(row - 1)(column - 1) // value in the cell above to the left

        // substitution cost (0 or 1)
        val substCost =
          if (s1(row - 1) == s2(column - 1)) 0
          else 1

        table(row)(column) = minimum(
          above + 1,            // deletion
          left + 1,             // insertion
          leftAbove + substCost // substitution
        )
      }
    }

    // extract the Levenshtein distance, i.e., the value in the bottom right corner
    table(s1.length)(s2.length)
  }
}
