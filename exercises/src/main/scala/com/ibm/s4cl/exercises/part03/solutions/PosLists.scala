package com.ibm.s4cl.exercises.part03.solutions

import scala.io.Source

class PosLists(treebankFile: String) {

  // a list of tuples representing all word tokens with their POS tags -- as read from file 'treebankFile'
  val listOfWordTokensWithPos: List[(String, String)] =
    // read all lines of the treebank file
    Source.fromURL(getClass.getResource(treebankFile))
      .getLines
      // ignore lines starting with '%%' (comments) or '#' (syntax)
      .filterNot(line => line.startsWith("%%") || line.startsWith("#"))
      // split each line at 'tabs' to get separate values for the columns of the input file
      .map(_.split("\t+"))
      // extract the word (first column) and its POS tag (second column)
      .map(splitLine => (splitLine(0), splitLine(1)))
      // convert the result (so far an iterator) into a list
      .toList

  /**
    * Extracts all word tokens that occur for a specified POS tag in file 'treebankFile'.
    *
    * @param pos the POS tag for which to extract all word tokens
    * @return returns the list of word tokens for the specified POS tag
    */
   def getTokensForSpecificPos(pos: String): List[String] =
    listOfWordTokensWithPos
      .filter(_._2 == pos)
      .map(_._1)

  /**
    * Extracts the number of word tokens that occur for a specified POS tag in file 'treebankFile'.
    *
    * @param pos the POS tag for which to extract all word tokens
    * @return returns the number of word tokens for the specified POS tag
    */
  def getNumberOfWordTokensForSpecificPos(pos: String): Int =
    getTokensForSpecificPos(pos).length

  /**
    * Extracts the list of words that occur for a specified POS tag in file 'treebankFile'. The
    * resulting word list does not contain any duplicates and it is sorted alphabetically.
    *
    * @param pos the POS tag for which to extract all words
    * @return returns the list of alphabetically sorted, distinct words for the specified POS tag
    */
  def getDistinctWordListForSpecificPos(pos: String): List[String] =
    getTokensForSpecificPos(pos)
      .sorted
      .distinct
}
