package com.ibm.s4cl.exercises.part03.solutions

import scala.io.Source

/**
  * Trigrams are contiguous sequences of three word tokens in a given text.
  *
  * @param textFile text file to extract trigrams from
  */
class Trigrams(textFile: String) {

  // An (incomplete) set of delimiter characters.
  val delimiter = "[ \t\n\r,.?!\\-:;()\\[\\]'\"/*#&$]+"

  // a list of triples representing all trigram tokens (in lower case) -- as read from file 'textFile'
  val trigrams: List[(String, String, String)] =
    // read all lines of the text file
    Source.fromURL(getClass.getResource(textFile))
      .getLines
      // tokenize all text
      .flatMap(_.split(delimiter))
      // ignore empty strings
      .filterNot(_.trim.isEmpty)
      // convert all tokens to lower case
      .map(_.toLowerCase())
      // extract a 'sliding window' view of size 3
      .sliding(3)
      // create trigram triples from each such 'sliding window'
      .map(trigram => (trigram(0), trigram(1), trigram(2)))
      // convert the result to a list of trigrams
      .toList

  /**
    * Predicts a plausible next word for a given bigram (word1, word2). The returned word is one of
    * the 'next' words with the highest frequency, as calculated from the list of trigrams extracted
    * from file 'textFile'.
    *
    * @param word1 the first word of the specified bigram
    * @param word2 the second word of the specified bigram
    * @return one of the most plausible 'next' words according to the calculated trigram frequencies
    */
  def predictNextWord(word1: String, word2: String): Option[String] = {
    // extract trigrams starting with the two words 'word1' and 'word2'
    val relevantTrigrams: Option[List[(String, String, String)]] =
      trigrams
        // group the list of trigrams by their first two words
        .groupBy(trigram => (trigram._1, trigram._2))
        // extract the 'grouped' entry that represents '(word1, word2)'; this results in an Option
        // containing a list of trigrams, which all start with 'word1 word2'
        .get(word1.toLowerCase, word2.toLowerCase)

    // depending on whether there are trigrams found that start with 'word1 word2'
    if (relevantTrigrams.isDefined) {
      // extract the list of 'third words' with the corresponding frequency how often they occur
      // as the third word token in the trigrams that start with 'word1 word2'
      val candidateWordsWithFrequencies: Map[String, Int] =
        // the Option 'relevantTrigrams' contains the list of trigrams that start with 'word1 word2'
        relevantTrigrams
          // extract the value of the Option, i.e., the list of trigrams starting with 'word1 word2'
          .get // return type: List[(String, String, String)]
          // consider the third word tokens only (ignore the first and second words)
          .map(_._3) // return type: List[String]
          // group together equal word tokens
          .groupBy(identity)
          // associate each word token with its frequency with which it occurs after 'word1 word2'
          .mapValues(words => words.length)

      // extract one of the words occurring with the highest frequency after 'word1 word2'
      val wordWithHighestFrequency = candidateWordsWithFrequencies.maxBy(_._2)._1

      // return the result word wrapped into an Option
      Some(wordWithHighestFrequency)
    } else {
      // return 'None', if there is no trigram starting with 'word1 word2'
      None
    }
  }
}
