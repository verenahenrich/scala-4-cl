package com.ibm.s4cl.exercises.part08

/**
  * Exercise: find location names in files.
  *
  * 1) Unzip the resources/anc.txt.zip file. It contains many text files from
  *    the American National Corpus.
  *
  * 2) Download http://download.geonames.org/export/dump/US.zip and unzip.
  *    This is a list of place names from the US. It is quite large. The format
  *    is documented in the readme file. The columns are tab separated. The
  *    second column is the place name, the 11th column the name of the state.
  *
  * 3) Read in the place names and the text files. When developing, only use a
  *    small subset of the files (use take(5) or similar).
  *
  * 4) Find instances of the place names in the text files. Note that there are
  *    many multi-word place names. Also, there are extremely short place names,
  *    such as "Y". Make sure to match only on full words.
  *
  * 5) When you get correct results, try running your code on the entire document
  *    collection. Does it complete in reasonable time? How can you speed up your
  *    code? Try using Scala's parallel collections where appropriate. How does
  *    that scale relative to the number of cores your machine has?
  *
  * 6) Give state-level statistics. How many location mentions of each state
  *    did you find?
  */
object LocationFinder {
  
  def main(args: Array[String]): Unit = {
    
  }
  
}
