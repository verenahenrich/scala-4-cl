package com.ibm.s4cl.exercises.part03

class NamedEntities(treebankFile: String) {

  // initialize an instance of PosLists, which reads file 'treebankFile' and creates a list of
  // tuples representing all word tokens with their POS tags; 'posLists.listOfWordTokensWithPos'
  // can/should be used to solve the current task
  val posLists = new PosLists(treebankFile)

  /**
    * Extracts all named entities (both single and multi token entities) from file 'treebankFile'.
    * This methods combines multi token named entities into one entry (by concatenating the tokens
    * with spaces in-between). Whether or not a named entity is represented by a single or by
    * multiple tokens is identified simply by their part-of-speech tags: If consecutive word tokens
    * have the same POS tag "NE", it is assumed they belong to the same named entity.
    *
    * For example, the output for the following list:
    *     List(("Peter", "NE"), ("likes", "V"), ("New", "NE"), ("York", "NE"))
    * is as follows:
    *     List("Peter", "New York")
    *
    * @return the list of named entities containing both single and multi token entities
    */
  def getNamedEntities(): List[String] = {

    // TODO: this method needs to be implemented

    List()
  }
}
