package com.ibm.s4cl.exercises.graded

import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source

class TweetReaderTest extends FlatSpec with Matchers {

  val twitterDataPath = "/Twitter_data.txt"
  val tweetReader = new TweetReader(Source.fromURL(getClass.getResource(twitterDataPath)))

  // sample authors used for testing
  val authorScalaLang = TweetAuthor("Scala", "scala_lang")
  val authorScalaJobz = TweetAuthor("Scala Jobz", "scalajobz")
  val authorScalaAtSO = TweetAuthor("ScalaAtStackOverflow", "ScalaAtSO")
  val authorScalaPLOW = TweetAuthor("Scala PLOW", "Scala_PLOW")
  val setOfAuthors = Set(authorScalaLang, authorScalaJobz, authorScalaAtSO, authorScalaPLOW)

  val expectedNumberOfTweets = 3754
  "The total number of extracted Tweets" should s"be $expectedNumberOfTweets" ignore {
    assert(tweetReader.tweets.size === expectedNumberOfTweets)
  }

  s"The $expectedNumberOfTweets Tweets" should "be sorted chronologically" ignore {
    assert(tweetReader.tweets.head.getPostedTime === "2016-10-01T13:20:29.000Z")
    assert(tweetReader.tweets(2).getPostedTime === "2016-10-01T14:17:46.000Z")
    assert(tweetReader.tweets(80).getPostedTime === "2016-10-02T12:35:38.000Z")
    assert(tweetReader.tweets.last.getPostedTime === "2016-11-15T22:30:29.000Z")
  }

  val expectedNumberOfOriginalTweets = 2110
  val expectedNumberOfRetweets = 1644
  "The total numbers of original Tweets and Retweets" should "be " +
    s"$expectedNumberOfOriginalTweets and $expectedNumberOfRetweets, respectively" ignore {

    assert(tweetReader.originalTweets.size === expectedNumberOfOriginalTweets)
    assert(tweetReader.retweets.size === expectedNumberOfRetweets)

    // check some specific entries in 'retweets'/'originalTweets' lists
    assert(tweetReader.retweets.head.getPostedTime === "2016-10-01T13:20:29.000Z")
    assert(tweetReader.originalTweets(1).getPostedTime === "2016-10-01T14:17:46.000Z")
    assert(tweetReader.originalTweets(45).getPostedTime === "2016-10-02T12:35:38.000Z")
    assert(tweetReader.retweets.last.getPostedTime === "2016-11-15T22:30:29.000Z")
  }

  val expectedNumberOfAuthors = 2614
  "The total number of authors" should s"be $expectedNumberOfAuthors" ignore {
    assert(tweetReader.authors.size === expectedNumberOfAuthors)
  }

  "Extracting Tweets by given authors" should "result in the expected numbers of Tweets" ignore {
    assert(tweetReader.getTweetsForAuthor(authorScalaLang).size === 6)
    assert(tweetReader.getTweetsForAuthor(authorScalaJobz).size === 10)
    assert(tweetReader.getTweetsForAuthor(authorScalaAtSO).size === 26)
    assert(tweetReader.getTweetsForAuthor(authorScalaPLOW).size === 97)

    assert(tweetReader.getTweetsForAuthors(setOfAuthors).size === 139)
  }

  "Extracting Tweets where given authors are mentioned" should "result in the expected numbers " +
    "of Tweets" ignore {
    assert(tweetReader.getTweetsWhereAuthorIsMentioned(authorScalaLang).size === 134)
    assert(tweetReader.getTweetsWhereAuthorIsMentioned(authorScalaJobz).size === 0)
    assert(tweetReader.getTweetsWhereAuthorIsMentioned(authorScalaAtSO).size === 3)
    assert(tweetReader.getTweetsWhereAuthorIsMentioned(authorScalaPLOW).size === 2)

    assert(tweetReader.getTweetsWhereAuthorsAreMentioned(setOfAuthors).size === 139)
  }

  // TODO comment in the following tests when appropriate

//  "Extracting authors mentioned in given Tweets" should "result in the expected numbers of authors" in {
//    val tweet1 = tweetReader.tweets(1)
//    assert(tweetReader.getMentionedAuthorsInTweet(tweet1).size === 2)
//
//    val tweet2 = tweetReader.tweets(2386)
//    val mentionedAuthors1 = tweetReader.getMentionedAuthorsInTweet(tweet2)
//    assert(mentionedAuthors1.size === 1)
//    assert(mentionedAuthors1.head == authorScalaLang)
//
//    val tweet3 = tweetReader.tweets(3352)
//    val mentionedAuthors2 = tweetReader.getMentionedAuthorsInTweet(tweet3)
//    assert(mentionedAuthors2.size === 3)
//    assert(mentionedAuthors2.contains(authorScalaLang))
//  }
//
//  "Extracting statistics on the most often retweeted Tweets" should "result in the expected numbers" in {
//    // mostRetweeted() without arguments should return all original Tweets, i.e., same elements
//    // as in the 'originalTweets' list (although in a different order)
//    assert(tweetReader.mostRetweeted().size === tweetReader.originalTweets.size)
//    assert(tweetReader.mostRetweeted().toSet === tweetReader.originalTweets.toSet)
//
//    assert(tweetReader.mostRetweeted().head.retweetCount === 630)
//
//    assert(tweetReader.mostRetweeted(600).size === 34)
//    assert(tweetReader.mostRetweeted(500).size === 261)
//    assert(tweetReader.mostRetweeted(400).size === 296)
//  }
//
//  "Extracting frequencies for specific hashtags" should "result in the expected numbers" in {
//    assert(tweetReader.getHashtagFrequency("#Scala") === 1163)
//    assert(tweetReader.getHashtagFrequency("#Java") === 152)
//    assert(tweetReader.getHashtagFrequency("#Haskell") === 22)
//    assert(tweetReader.getHashtagFrequency("#Python") === 45)
//  }
//
//  "Calculating the similarity between two tweets based on hashtags" should "result in the expected similarity number" in {
//    val tweet1: OriginalTweet = tweetReader.originalTweets(8)
//    val tweet2: OriginalTweet = tweetReader.originalTweets(10)
//    val tweet3: OriginalTweet = tweetReader.originalTweets(14)
//    assert(tweetReader.hashtagSimilarity(tweet1, tweet2) === 0.25)
//    assert(tweetReader.hashtagSimilarity(tweet2, tweet1) === 0.25)
//    assert(tweetReader.hashtagSimilarity(tweet1, tweet3) === 0.5)
//    assert(tweetReader.hashtagSimilarity(tweet2, tweet3) === 0.2)
//  }
//
//  "Finding most similar tweets based on hashtags" should "result in the expected tweets" in {
//    val tweet: OriginalTweet = tweetReader.originalTweets(45)
//    val mostSimilar = tweetReader.findMostSimilarTweetsByHashtags(tweet)
//    assert(mostSimilar.size === 1)
//
//    val expectedAuthor = TweetAuthor("Fran Scantlebury","FranScan")
//    val expectedTimestamp = "2016-10-06T03:30:23.000Z"
//    val expectedHashTags = List("#functionalprogramming", "#haskell", "#scala", "#erlang")
//    assert(mostSimilar.head.getAuthor === expectedAuthor)
//    assert(mostSimilar.head.getPostedTime === expectedTimestamp)
//    assert(expectedHashTags.forall(hashtag => mostSimilar.head.getHashtags.contains(hashtag)))
//  }

}
