package com.ibm.s4cl.exercises.part02.solutions

import org.scalatest.{FlatSpec, Matchers}

class FileStatsWithListsTest extends FlatSpec with Matchers {

  val numTokens = 792858
  s"The number of tokens in file ${FileStatsWithLists.fileName}" should s"be $numTokens" in {
    assert(FileStatsWithLists.tokens.length === numTokens)
  }

  val numDistinctTokens = 17563
  "The number of distinct tokens" should s"be $numDistinctTokens" in {
    assert(FileStatsWithLists.tokens.distinct.length === numDistinctTokens)
  }

  val numNormalizeTokens = 15397
  "The number of normalized tokens" should s"be $numNormalizeTokens" in {
    assert(FileStatsWithLists.normalizedTokens.length === numNormalizeTokens)
  }

  val numLadies = 33
  "The number of distinct names following an occurrence of 'Lady'" should s"be $numLadies" in {
    val ladies = FileStatsWithLists.extractPerson("Lady")
    assert(ladies.length === numLadies)
    assert(List("Elliot", "Russell", "Mary", "Wentworth").forall(lady => ladies.contains(lady)))
  }

  val numMr = 113
  "The number of distinct names following an occurrence of 'Mr'" should s"be $numMr" in {
    val misters = FileStatsWithLists.extractPerson("Mr")
    assert(misters.length === numMr)
    assert(List("Elliot", "Shepherd", "Musgrove", "Robinson").forall(mister => misters.contains(mister)))
  }

}
