package com.ibm.s4cl.exercises.part02.solutions

import org.scalatest.{FlatSpec, Matchers}

class PalindromeTest extends FlatSpec with Matchers {

  "Strings of length '0' and '1'" should "be identified as palindromes (by method " +
    "isPalindromeWithBuiltIn)" in {
    val palindromes = List("", "s", "S", "a")
    palindromes.foreach(palindrome => assert(Palindrome.isPalindromeWithBuiltIn(palindrome)))
  }

  "Words such as 'radar', 'madam', 'eye', 'kayak', and 'testset'" should
    "be identified as palindromes (by method isPalindromeWithBuiltIn)" in {
    val palindromes = List("radar", "madam", "eye", "kayak", "testset")
    palindromes.foreach(palindrome => assert(Palindrome.isPalindromeWithBuiltIn(palindrome)))
  }

  "Arbitrary words such as 'scala', 'Tübingen', 'Verena', and 'Thilo'" should
    "not be identified as palindromes (by method isPalindromeWithBuiltIn)" in {
    val words = List("scala", "Tübingen", "Verena", "Thilo")
    val stringIsPalindrome = false
    words.foreach(word => assert(!Palindrome.isPalindromeWithBuiltIn(word)))
  }

  "Strings of length '0' and '1'" should "be identified as palindromes (by method isPalindrome)" in {
    val palindromes = List("", "s", "S", "a")
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindromeRecursively(palindrome), s"; Error in classifying '$palindrome'"))
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindromeNonRecursively(palindrome), s"; Error in classifying '$palindrome'"))
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindromeNonRecursivelyWithFor(palindrome), s"; Error in classifying '$palindrome'"))
  }

  "Words such as 'radar', 'madam', 'eye', 'kayak', and 'testset'" should
    "be identified as palindromes (by method isPalindrome)" in {
    val palindromes = List("radar", "madam", "eye", "kayak", "testset")
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindromeRecursively(palindrome), s"; Error in classifying '$palindrome'"))
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindromeNonRecursively(palindrome), s"; Error in classifying '$palindrome'"))
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindromeNonRecursivelyWithFor(palindrome), s"; Error in classifying '$palindrome'"))
  }

  "Arbitrary words such as 'scala', 'Tübingen', 'Verena', and 'Thilo'" should
    "not be identified as palindromes (by method isPalindrome)" in {
    val words = List("scala", "Tübingen", "Verena", "Thilo")
    val stringIsPalindrome = false
    words.foreach(word =>
      assert(!Palindrome.isPalindromeRecursively(word), s"; Error in classifying '$word'"))
    words.foreach(word =>
      assert(!Palindrome.isPalindromeNonRecursively(word), s"; Error in classifying '$word'"))
    words.foreach(word =>
      assert(!Palindrome.isPalindromeNonRecursivelyWithFor(word), s"; Error in classifying '$word'"))
  }
}
