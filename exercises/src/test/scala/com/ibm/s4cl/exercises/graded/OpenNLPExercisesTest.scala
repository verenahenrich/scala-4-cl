package com.ibm.s4cl.exercises.graded

import com.ibm.s4cl.exercises.graded.OpenNLPExercises._
import org.scalatest.FunSuite

class OpenNLPExercisesTest extends FunSuite {

  val numberOfSentences = 5588
  test(s"The total number of sentences should be $numberOfSentences") {
    assert(sentences.size === numberOfSentences)
    assert(sentences.head.tokens.size === 34) // first sentence consists of 34 tokens
  }

  test("Get the list of \"sentence span\"/\"number of token\" pairs") {
    val sentencesByTokenCount = getSentencesByTokenCount()
    assert(sentencesByTokenCount.size === numberOfSentences)
    assert(sentencesByTokenCount.head._2 === 1) // shortest sentence
    assert(sentencesByTokenCount.last._2 === 191) // longest sentence
    assert(sentencesByTokenCount(numberOfSentences-2)._2 === 135) // second longest sentence
  }

  val numberOfNNCompounds = 243
  test(s"The total number of noun-noun compounds should be $numberOfNNCompounds") {
    assert(getNounNounCompounds().size === numberOfNNCompounds)
  }

  val numberOfTokens = 142612

  val numberOfTokensWithAmbiguousPos = 1121
  test(s"$numberOfTokensWithAmbiguousPos tokens should be tagged with more than one POS") {
    val tokensWithMultiplePos = getTokensWithMultiplePos()
    assert(tokensWithMultiplePos.size === numberOfTokensWithAmbiguousPos)

    // token "name" occurs as "NN" and as "VB"
    assert(tokensWithMultiplePos("name") === Set("NN", "VB"))

    // token "fancy" occurs as "NN", as "VB", and as "JJ"
    assert(tokensWithMultiplePos("fancy") === Set("NN", "VB", "JJ"))

    // token "unfolded" occurs as "NN", as "VBD", and as "VBN"
    assert(tokensWithMultiplePos("unfolded") === Set("JJ", "VBD", "VBN"))
  }

  test("Get POS trigrams with corresponding token trigrams") {
    val trigrams = getPosTrigramsWithCorrespondingTokens()
    assert(trigrams.size === 7592)

    // extract all occurring token trigrams for POS combination "DT"-"JJ"-"NN"
    assert(trigrams("DT", "JJ", "NN").size === 1021)

    // extract all occurring token trigrams for POS combination "NN"-"CC"-"NN"
    assert(trigrams("NN", "CC", "NN").size === 233)
  }

}
