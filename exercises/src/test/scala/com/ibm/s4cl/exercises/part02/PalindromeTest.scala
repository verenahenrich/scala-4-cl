/**
  * This code is part of the programming course "Scala Programming for Computational Linguistics"
  * by Verena Henrich & Thilo Götz. Contact: scala4cl@gmail.com
  */
package com.ibm.s4cl.exercises.part02

import org.scalatest.{FlatSpec, Matchers}

class PalindromeTest extends FlatSpec with Matchers {

  "Strings of length '0' and '1'" should "be identified as palindromes (by method " +
    "isPalindromeWithBuiltIn)" in {
    val palindromes = List("", "s", "S", "a")
    palindromes.foreach(palindrome => assert(Palindrome.isPalindromeWithBuiltIn(palindrome)))
  }

  "Words such as 'radar', 'madam', 'eye', 'kayak', and 'testset'" should
    "be identified as palindromes (by method isPalindromeWithBuiltIn)" in {
    val palindromes = List("radar", "madam", "eye", "kayak", "testset")
    palindromes.foreach(palindrome => assert(Palindrome.isPalindromeWithBuiltIn(palindrome)))
  }

  "Arbitrary words such as 'scala', 'Tübingen', 'Verena', and 'Thilo'" should
    "not be identified as palindromes (by method isPalindromeWithBuiltIn)" in {
    val words = List("scala", "Tübingen", "Verena", "Thilo")
    words.foreach(word => assert(!Palindrome.isPalindromeWithBuiltIn(word)))
  }

  "Strings of length '0' and '1'" should "be identified as palindromes (by method isPalindrome)" ignore {
    val palindromes = List("", "s", "S", "a")
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindrome(palindrome), s"; Error in classifying '$palindrome'"))
  }

  "Words such as 'radar', 'madam', 'eye', 'kayak', and 'testset'" should
    "be identified as palindromes (by method isPalindrome)" ignore {
    val palindromes = List("radar", "madam", "eye", "kayak", "testset")
    palindromes.foreach(palindrome =>
      assert(Palindrome.isPalindrome(palindrome), s"; Error in classifying '$palindrome'"))
  }

  "Arbitrary words such as 'scala', 'Tübingen', 'Verena', and 'Thilo'" should
    "not be identified as palindromes (by method isPalindrome)" ignore {
    val words = List("scala", "Tübingen", "Verena", "Thilo")
    words.foreach(word =>
      assert(!Palindrome.isPalindrome(word), s"; Error in classifying '$word'"))
  }
}
