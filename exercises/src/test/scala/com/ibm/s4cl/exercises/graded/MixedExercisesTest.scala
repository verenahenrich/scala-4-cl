package com.ibm.s4cl.exercises.graded

import org.scalatest.FunSuite

import MixedExercises._

class MixedExercisesTest extends FunSuite {

  test("Compute the size of a list tail-recursively") {
    val list1 = List("a", "b", "c")
    val list2 = List(1, 2, 3, 4, 5)

    assert(listSize(list1) === 3)
    assert(listSize(list2) === 5)
    // TODO: uncomment
//    assert(listSizeTailRec(list1) === 3)
//    assert(listSizeTailRec(list2) === 5)
  }

  test("Switch arguments to a two-argument function.") {
    def charAt(s: String, pos: Int) = s.charAt(pos)
    // TODO: uncomment
//    assert(charAt("foo", 1) === argswitch(charAt)(1, "foo"))
  }

}
