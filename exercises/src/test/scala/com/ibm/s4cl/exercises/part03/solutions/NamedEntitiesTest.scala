package com.ibm.s4cl.exercises.part03.solutions

import org.scalatest.{FlatSpec, Matchers}

class NamedEntitiesTest extends FlatSpec with Matchers {

  val namedEntities = new NamedEntities("/tuebadz_1-50.utf8")

  val expectedNumberOfNamedEntities = 55
  "The total number of extracted named entities" should s"be $expectedNumberOfNamedEntities" in {
    assert(namedEntities.getNamedEntities.length === expectedNumberOfNamedEntities)
  }

  "Extracting the numbers of words for specified parts of speech" should
    "result in the correct numbers of words" in {

    val listOfNamedEntities = namedEntities.getNamedEntities
    assert(listOfNamedEntities.head === "Danzig")
    assert(listOfNamedEntities(1) === "Ute Wedemeier")
    assert(listOfNamedEntities(2) === "Bremen")
    assert(listOfNamedEntities(3) === "Hans Taake")
  }

}
