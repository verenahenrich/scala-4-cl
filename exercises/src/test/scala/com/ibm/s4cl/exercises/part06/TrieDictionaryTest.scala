package com.ibm.s4cl.exercises.part06

import org.scalatest.FunSuite

import scala.io.Source

class TrieDictionaryTest extends FunSuite {
  
  // Basic test, try this one first
  test("Simple dictionary entries") {
    val dictWords = Array[String]("foo", "bar", "bam", "bbq")
    val nonDictWords = Array[String]("", "fo", "fooo", "ba", "b")
    val dict = TrieDictionary().addAll(dictWords)
    dictWords
      .foreach(s => assert(dict.find(s), s"$s should be in dictionary"))
    nonDictWords
      .foreach(s => assert(!dict.find(s), s"$s should NOT be in dictionary"))
  }
  
  // This one tests if you implemented entries correctly that are prefixes of other entries.
  test("Prefix dictionary entries") {
    val dictWords = Array[String]("a", "aa", "aab")
    val nonDictWords = Array[String]("", "ab", "aaab", "ba")
    val dict = TrieDictionary().addAll(dictWords)
    dictWords
      .foreach(s => assert(dict.find(s), s"$s should be in dictionary"))
    nonDictWords
      .foreach(s => assert(!dict.find(s), s"$s should NOT be in dictionary"))
    assert(dict.findAll("a").size == 1)
    assert(dict.findAll("aa").size == 3)
    assert(dict.findAll("aab").size == 4)
  }
  
  // Depending on your implementation and your hardware, this one may be slow (takes about a second
  // or so for me)
  test("Finding Lizzy") {
    val is = getClass.getResourceAsStream("/pnp.txt")
    val dict = TrieDictionary("Lizzy")
    val spans = Source
      .fromInputStream(is)
      .getLines()
      .flatMap(line => {
        dict
          .findAll(line)
          .map(span => {
            println(TrieDictionary.printSpan(span, line))
            span
          })
      })
    assert(spans.size == 95, "There should be 95 occurences of Lizzy.")
  }
}
