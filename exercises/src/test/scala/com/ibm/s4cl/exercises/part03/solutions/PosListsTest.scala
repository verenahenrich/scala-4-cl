package com.ibm.s4cl.exercises.part03.solutions

import org.scalatest.{FlatSpec, Matchers}

class PosListsTest extends FlatSpec with Matchers {

  val posLists = new PosLists("/tuebadz_1-50.utf8")

  val expectedNumberOfWordTokens = 942
  "The total number of extracted word tokens" should s"be $expectedNumberOfWordTokens" in {
    assert(posLists.listOfWordTokensWithPos.length === expectedNumberOfWordTokens)
  }

  "Extracting the numbers of words for specified parts of speech" should
    "result in the correct numbers of words" in {
    assert(posLists.getNumberOfWordTokensForSpecificPos("NN") === 180) // nouns
    assert(posLists.getNumberOfWordTokensForSpecificPos("NE") === 75) // proper nouns
    assert(posLists.getNumberOfWordTokensForSpecificPos("ADJA") === 44) // attributive adjectives
    assert(posLists.getNumberOfWordTokensForSpecificPos("ADJD") === 14) // adverbial or predicative adjectives
    assert(posLists.getNumberOfWordTokensForSpecificPos("ART") === 98) // articles
    assert(posLists.getNumberOfWordTokensForSpecificPos("VVINF") === 13) // infinitive verbs
    assert(posLists.getNumberOfWordTokensForSpecificPos("VAFIN") === 36) // auxiliary finite verbs
    assert(posLists.getNumberOfWordTokensForSpecificPos("PTKVZ") === 2) // separated verb particles
    assert(posLists.getNumberOfWordTokensForSpecificPos("PDAT") === 3) // attributive possessive pronouns
    assert(posLists.getNumberOfWordTokensForSpecificPos("CARD") === 12) // cardinal numbers

    // empty or unknown POS tag
    assert(posLists.getNumberOfWordTokensForSpecificPos("") === 0)
    assert(posLists.getNumberOfWordTokensForSpecificPos("**") === 0)
  }

  "Extracting all distinct words for specified parts of speech" should
    "result in the correct word lists" in {

    // separated verb particles
    assert(posLists.getDistinctWordListForSpecificPos("PTKVZ") === List("ab", "zu"))

    // attributive possessive pronouns
    assert(posLists.getDistinctWordListForSpecificPos("PDAT") === List("diese", "diesem", "dieser"))

    // cardinal numbers
    assert(posLists.getDistinctWordListForSpecificPos("CARD") ===
      List("1", "10.000", "165.000", "2", "20.000", "3", "86", "fünf", "hundert"))

    // empty or unknown POS tag
    assert(posLists.getDistinctWordListForSpecificPos("") === List())
    assert(posLists.getDistinctWordListForSpecificPos("**") === List())
  }
}
