package com.ibm.s4cl.exercises.part06

import com.ibm.s4cl.exercises.part06.ListExercise._
import org.scalatest.FunSuite

class ListExerciseTest extends FunSuite {
  
  val stringList = List("aa", "bb", "cc")
  val stringListR = List("cc", "bb", "aa")
  
  val intList = (1 to 10).toList
  val intListR = intList.reverse
  
  test("Testing naive reverse") {
    assert(stringList == naiveReverse(naiveReverse(stringList)), "Reverse is its own inverse")
    assert(stringList == naiveReverse(stringListR), "Reversing string list")
    assert(intListR == naiveReverse(intList), "Reversing int list")
  }
  
  //TODO: do the same tests with your reverse implementation
  
}
