package com.ibm.s4cl.exercises.part03.solutions

import org.scalatest.{FlatSpec, Matchers}

class TrigramsTest extends FlatSpec with Matchers {

  val trigrams = new Trigrams("/austen-complete.txt")

  val expectedNumberOfTrigrams = 792856
  "The total number of extracted trigrams" should s"be $expectedNumberOfTrigrams" in {
    assert(trigrams.trigrams.length === expectedNumberOfTrigrams)
  }

  "Calling method 'getNumberOfWordTokensForSpecificPos' with empty String arguments" should
    "return an empty result" in {
    assert(trigrams.predictNextWord("", "") === None)
  }

  "Attempting to predict the next word after the two words 'Thilo' + 'Verena' which never " +
    "occur as a bigram in the input text" should "return an empty result" in {
    assert(trigrams.predictNextWord("Thilo", "Verena") === None)
  }

  "Predicting the next words for several specified bigrams" should
    "result in the correct words (single 'next' word with the highest frequency)" in {
    assert(trigrams.predictNextWord("my", "opinion") === Some("of"))
    assert(trigrams.predictNextWord("you", "seem") === Some("to"))
    assert(trigrams.predictNextWord("i", "give") === Some("you"))
    assert(trigrams.predictNextWord("if", "there") === Some("is"))
    assert(trigrams.predictNextWord("very", "different") === Some("from"))
    assert(trigrams.predictNextWord("an", "interesting") === Some("object"))
    assert(trigrams.predictNextWord("read", "the") === Some("letter"))
  }

  "Predicting the next words for several specified bigrams" should
    "result in the correct words (several 'next' words with the same frequency)" in {

    val sheImmediately = trigrams.predictNextWord("she", "immediately")
    assert(sheImmediately === Some("said") || sheImmediately === Some("felt"))

    val aSad = trigrams.predictNextWord("a", "sad")
    assert(aSad === Some("thing") || aSad === Some("change"))

    val iExpected = trigrams.predictNextWord("I", "expected")
    assert(iExpected === Some("to") || iExpected === Some("you"))
  }
}
