package com.ibm.s4cl.exercises.part02

import org.scalatest.{FlatSpec, Matchers}

class CompoundSplitterTest extends FlatSpec with Matchers {

  // initialize compound splitters (for English and German)
  val splitterEn = new CompoundSplitter("simplex_nouns_en.dict")
  val splitterDe = new CompoundSplitter("simplex_nouns_de.dict")

  "The empty string" should "not be labelled as a compound" ignore {
    assert(!splitterEn.isCompound(""))
    assert(!splitterDe.isCompound(""))
  }

  "Simplex English nouns such as 'apple', 'man', and 'table'" should "not be split" ignore {
    assert(!splitterEn.isCompound("horse"))
    assert(!splitterEn.isCompound("man"))
    assert(!splitterEn.isCompound("table"))
  }

  "Simplex German nouns such as 'Apfel', 'Mann', and 'Tisch'" should "not be split" ignore {
    assert(!splitterDe.isCompound("Apfel"))
    assert(!splitterDe.isCompound("Mann"))
    assert(!splitterDe.isCompound("Tisch"))
  }

  "English compounds where not all constituent parts exist in the list of simplex nouns" should
    "not be split" ignore {

    assert(!splitterEn.isCompound("cranberry")) // bound morpheme
    assert(!splitterEn.isCompound("redhead"))   // adjective + noun
  }

  "German compounds where not all constituent parts exist in the list of simplex nouns" should
    "not be split" ignore {

    assert(!splitterDe.isCompound("Himbeere"))   // bound morpheme + noun
    assert(!splitterDe.isCompound("Zentimeter")) // bound morpheme + noun
    assert(!splitterDe.isCompound("Aktivkohle")) // adjective + noun
  }

  "The English compound 'snowman'" should "be split into 'snow+man'" ignore {
    assert(splitterEn.isCompound("snowman"))
    assert(splitterEn.split("snowman") === List(List("snow", "man")))
  }

  "The English compound 'horseshoe'" should "be split into 'horse+shoe'" ignore {
    assert(splitterEn.isCompound("horseshoe"))
    assert(splitterEn.split("horseshoe") === List(List("horse", "shoe")))
  }

  "The German compound 'Schneemann'" should "be split into 'Schnee+Mann'" ignore {
    assert(splitterDe.isCompound("Schneemann"))
    assert(splitterDe.split("Schneemann") === List(List("Schnee", "Mann")))
  }

  "The German compound 'Apfelbaum'" should "be split into 'Apfel+Baum'" ignore {
    assert(splitterDe.isCompound("Apfelbaum"))
    assert(splitterDe.split("Apfelbaum") === List(List("Apfel", "Baum")))
  }

  "The German compound 'Weltreise'" should "be split into 'Welt+Reise'" ignore {
    assert(splitterDe.isCompound("Weltreise"))
    assert(splitterDe.split("Weltreise") === List(List("Welt", "Reise")))
  }

  "The German compound 'Autobahnanschlussstelle'" should
    "be split into 'Auto+Bahn+Anschluss+Stelle'" ignore {

    assert(splitterDe.isCompound("Autobahnanschlussstelle"))
    assert(splitterDe.split("Autobahnanschlussstelle") ===
      List(List("Auto", "Bahn", "Anschluss", "Stelle")))
  }

  "The German compound 'Druckerzeugnis'" should "be split" ignore {
    assert(splitterDe.isCompound("Druckerzeugnis"))
    assert(splitterDe.split("Druckerzeugnis") ===
      List(List("Druck", "Erzeugnis"), List("Drucker", "Zeugnis")))
  }
}
