package com.ibm.s4cl.exercises.part04.solutions

import org.scalatest.{FlatSpec, Matchers}

class TripleTreeTest extends FlatSpec with Matchers {

  // the trees are based on an example sentence taken from the PennTreebank
  val leaf1 = "Mr."
  val leaf2 = "Vinken"
  val tripleTree1 = (leaf1, "NP", leaf2)
  val tripleTree2 = ("Elsevier", "NP", "N.V.")
  val tripleTree3 = ("of", "PP", tripleTree2)
  val tripleTree4 = ("chairman", "NP", tripleTree3)
  val tripleTree5 = ("is", "VP", tripleTree4)
  val tripleTree6 = (tripleTree1, "S", tripleTree5)

  // expected results
  val leaf1ToString = "(Mr.)"
  val leaf2ToString = "(Vinken)"
  val tripleTree1ToString = "((Mr.) NP (Vinken))"
  val tripleTree2ToString = "((Elsevier) NP (N.V.))"
  val tripleTree3ToString = "((of) PP ((Elsevier) NP (N.V.)))"
  val tripleTree4ToString = "((chairman) NP ((of) PP ((Elsevier) NP (N.V.))))"
  val tripleTree5ToString = "((is) VP ((chairman) NP ((of) PP ((Elsevier) NP (N.V.)))))"
  val tripleTree6ToString = "(((Mr.) NP (Vinken)) S ((is) VP ((chairman) NP ((of) PP ((Elsevier) NP (N.V.))))))"

  "Converting leaves into strings" should "equal the predefined result strings" in {
    assert(TripleTree.tripleTreeToString(leaf1) === leaf1ToString)
    assert(TripleTree.tripleTreeToString(leaf2) === leaf2ToString)
  }

  "Converting simple phrases into strings" should "equal the predefined result strings" in {
    assert(TripleTree.tripleTreeToString(tripleTree1) === tripleTree1ToString)
    assert(TripleTree.tripleTreeToString(tripleTree2) === tripleTree2ToString)
  }

  "Converting nested phrases into strings" should "equal the predefined result strings" in {
    assert(TripleTree.tripleTreeToString(tripleTree3) === tripleTree3ToString)
    assert(TripleTree.tripleTreeToString(tripleTree4) === tripleTree4ToString)
    assert(TripleTree.tripleTreeToString(tripleTree5) === tripleTree5ToString)
    assert(TripleTree.tripleTreeToString(tripleTree6) === tripleTree6ToString)
  }

  "Extracting all PPs from the sample sentence" should "result in 1 phrase" in {
    val PPs = TripleTree.extractPhrasesFromTripleTree(tripleTree6, "PP")
    assert(PPs.length === 1)
    assert(PPs.head === tripleTree3ToString)
  }

  "Extracting all NPs from the sample sentence" should
    "result in 2 phrases (nested NPs should not be extracted)" in {

    val NPs = TripleTree.extractPhrasesFromTripleTree(tripleTree6, "NP")
    assert(NPs.length === 2)
    assert(NPs.head === tripleTree1ToString)
    assert(NPs(1) === tripleTree4ToString)
  }

  "Trying to extract VPs from an NP" should "return an empty result" in {
    assert(TripleTree.extractPhrasesFromTripleTree(tripleTree4, "VP").length === 0)
  }

}
