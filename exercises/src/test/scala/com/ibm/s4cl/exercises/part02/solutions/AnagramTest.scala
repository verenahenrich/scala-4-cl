package com.ibm.s4cl.exercises.part02.solutions

import org.scalatest.{FlatSpec, Matchers}

class AnagramTest extends FlatSpec with Matchers {

  "'phase'" should "have one anagram: 'shape'" in {
    assert(Anagram.getAnagrams("phase") === List("shape"))
  }

  "'demo'" should "have two anagrams: 'mode' and 'dome'" in {
    assert(Anagram.getAnagrams("demo").sorted === List("dome", "mode"))
  }

  "'altering'" should "have four anagrams: 'alerting', 'integral', 'relating', 'triangle'" in {
    assert(Anagram.getAnagrams("altering").sorted ===
      List("alerting", "integral", "relating", "triangle"))
  }
}
