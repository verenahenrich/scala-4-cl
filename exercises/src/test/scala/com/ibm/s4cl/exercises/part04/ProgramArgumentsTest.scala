package com.ibm.s4cl.exercises.part04

import org.scalatest.{FlatSpec, Matchers}

class ProgramArgumentsTest extends FlatSpec with Matchers {

  "Parsing arguments '-h' and '--help'" should "return the list of valid arguments" ignore {
    assert(ProgramArguments.parseArgument("-h", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("--help", None) === ProgramArguments.validArguments)
  }

  "Parsing argument '--version'" should s"return '${ProgramArguments.version}'" ignore {
    assert(ProgramArguments.parseArgument("--version", None) === ProgramArguments.version)
  }

  "Parsing arguments '-l' and '--lang' with appropriate values" should
    "set the language accordingly" ignore {

    val languageEn = "en"
    val expectedResultEn = "Setting the language to English"
    assert(ProgramArguments.parseArgument("-l", languageEn) === expectedResultEn)
    assert(ProgramArguments.parseArgument("--lang", languageEn) === expectedResultEn)

    val languageDe = "de"
    val expectedResultDe = "Setting the language to German"
    assert(ProgramArguments.parseArgument("-l", languageDe) === expectedResultDe)
    assert(ProgramArguments.parseArgument("--lang", languageDe) === expectedResultDe)
  }

  "Parsing arguments '-l' and '--lang' with inappropriate values" should
    "return the list of allowed languages" ignore {

    val expectedResult = "Invalid language, allowed values are: 'en' and 'de'"
    assert(ProgramArguments.parseArgument("-l", "fr") === expectedResult)
    assert(ProgramArguments.parseArgument("--lang", "es") === expectedResult)
  }

  "Parsing arguments '-i' and '--input' with appropriate values" should
    "set the input files accordingly" ignore {

    val fileName = "dummy_file.txt"
    val expectedResult = s"Set input file '$fileName'"
    assert(ProgramArguments.parseArgument("-i", fileName) === expectedResult)
    assert(ProgramArguments.parseArgument("--input", fileName) === expectedResult)
  }

  "Parsing arguments '-p' and '--pages' with appropriate values" should
    "set the number of pages accordingly" ignore {

    val fivePages = 5
    val expectedResult5 = s"Setting the number of pages to $fivePages"
    assert(ProgramArguments.parseArgument("-p", fivePages) === expectedResult5)
    assert(ProgramArguments.parseArgument("--pages", fivePages) === expectedResult5)

    val twentyPages = 20
    val expectedResult20 = s"Setting the number of pages to $twentyPages"
    assert(ProgramArguments.parseArgument("-p", twentyPages) === expectedResult20)
    assert(ProgramArguments.parseArgument("--pages", twentyPages) === expectedResult20)
  }

  "Parsing arguments '-p' and '--pages' with inappropriate values" should
    "return an appropriate message" ignore {

    val notNegative = "The number of pages has to be positive"
    assert(ProgramArguments.parseArgument("-p", -2) === notNegative)

    val below30 = "The number of pages has to be below 30"
    assert(ProgramArguments.parseArgument("--pages", 50) === below30)
  }

  "Parsing arguments '-l', '-i', and '-p' with inappropriate values (wrong data types)" should
    "return the list of valid arguments" ignore {

    assert(ProgramArguments.parseArgument("-l", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("-i", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("-p", None) === ProgramArguments.validArguments)

    assert(ProgramArguments.parseArgument("--lang", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("--input", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("--pages", None) === ProgramArguments.validArguments)

    assert(ProgramArguments.parseArgument("-l", 'e') === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("-i", 10) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("-p", "10") === ProgramArguments.validArguments)
  }

  "Parsing invalid arguments" should "return the list of valid arguments" ignore {
    assert(ProgramArguments.parseArgument("--l", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("-pages", None) === ProgramArguments.validArguments)
    assert(ProgramArguments.parseArgument("-y", None) === ProgramArguments.validArguments)
  }
}
