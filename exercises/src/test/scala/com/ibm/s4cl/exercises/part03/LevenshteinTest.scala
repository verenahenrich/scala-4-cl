/**
  * This code is part of the programming course "Scala Programming for Computational Linguistics"
  * by Verena Henrich & Thilo Götz. Contact: scala4cl@gmail.com
  */
package com.ibm.s4cl.exercises.part03

import org.scalatest.{FlatSpec, Matchers}

class LevenshteinTest extends FlatSpec with Matchers {

  "Calculating Levenshtein on equal strings" should "always result in a distance of '0'" ignore {
    assert(Levenshtein.distance("S", "S") === 0)
    assert(Levenshtein.distance("scala", "scala") === 0)
  }

  "Calculating Levenshtein with empty strings" should "work properly" ignore {
    assert(Levenshtein.distance("", "") === 0)
    assert(Levenshtein.distance("S", "") === 1)
    assert(Levenshtein.distance("", "S") === 1)
    assert(Levenshtein.distance("scala", "") === 5)
    assert(Levenshtein.distance("", "scala") === 5)
  }

  "Calculating Levenshtein on distinct strings" should "result in the defined distances" ignore {
    assert(Levenshtein.distance("Java", "Scala") === 3)
    assert(Levenshtein.distance("Scala", "Java") === 3)
    assert(Levenshtein.distance("Verena", "Thilo") === 6)
    assert(Levenshtein.distance("kitten", "sitting") === 3)
    assert(Levenshtein.distance("Böblingen", "Tübingen") === 3)
    assert(Levenshtein.distance("Data Structures and Algorithms for Language Processing",
                                "Scala Programming for Computational Linguistics") === 41)
  }
}
