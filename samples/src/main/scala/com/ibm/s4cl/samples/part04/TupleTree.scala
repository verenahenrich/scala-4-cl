package com.ibm.s4cl.samples.part04

object TupleTree {

  def main(args: Array[String]): Unit = {
    val leaf1 = "A"
    val leaf2 = "B"
    val tupleTree1 = (leaf1, leaf2)
    val tupleTree2 = ("L1", "L2")
    val tupleTree3 = (tupleTree1, "C")
    val tupleTree4 = (tupleTree1, ("C", "D"))
    val tupleTree5 = (tupleTree1, (("C", "D"), "E"))

    println("******** matchTupleTree *******")
    println(matchTupleTree(leaf1))
    println(matchTupleTree(leaf2))
    println(matchTupleTree(tupleTree1))
    println(matchTupleTree(tupleTree2))
    println(matchTupleTree(tupleTree3))
    println(matchTupleTree(tupleTree4))
    println(matchTupleTree(tupleTree5))

    println("******** tupleTreeToString *******")
    println(tupleTreeToString(tupleTree1))
    println(tupleTreeToString(tupleTree2))
    println(tupleTreeToString(tupleTree3))
    println(tupleTreeToString(tupleTree4))
    println(tupleTreeToString(tupleTree5))

    println("******** reverseTupleTree *******")
    println(tupleTreeToString(reverseTupleTree(tupleTree1)))
    println(tupleTreeToString(reverseTupleTree(tupleTree2)))
    println(tupleTreeToString(reverseTupleTree(tupleTree3)))
    println(tupleTreeToString(reverseTupleTree(tupleTree4)))
    println(tupleTreeToString(reverseTupleTree(tupleTree5)))
  }

  def matchTupleTree(tree: Any) = tree match {
    case "A" => "leaf 'A'" // constant pattern
    case leaf: String => s"any other leaf: '$leaf'" // typed variable pattern
    case ("A", _) => "left leaf is 'A'" // tuple pattern with a constant and a wildcard
    case (firstLeaf: String, secondLeaf: String) => // tuple pattern with two typed variables
      s"branch with two leaves: '$firstLeaf' + '$secondLeaf'"
    case (_, _) => "any other branch" // tuple pattern with two wildcards
  }

  def tupleTreeToString(tree: Any): String = tree match {
    case leaf: String => leaf
    case (left: Any, right: Any) => s"${tupleTreeToString(left)} ${tupleTreeToString(right)}"
  }

  def reverseTupleTree(tree: Any): Any = tree match {
    case leaf: String => leaf
    case (left: Any, right: Any) => (reverseTupleTree(right), reverseTupleTree(left))
  }

}
