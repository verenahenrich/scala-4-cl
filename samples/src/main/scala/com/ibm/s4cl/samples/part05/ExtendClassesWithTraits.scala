package com.ibm.s4cl.samples.part05

import java.nio.charset.{CharsetEncoder, StandardCharsets}
import java.util.Base64

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Sample code to demonstrate how to extend existing classes with traits

/**
  * Encode the value of toString() as Base64
  */
trait AsBase64 {
  def asBase64String(): String = {
    Base64.getEncoder.encodeToString(
      toString.getBytes(StandardCharsets.UTF_8))
  }
}

/**
  * A part that can output Base64.
  * @param part
  */
class Part0(part: String) extends AsBase64 {
  override def toString: String = part
}

/**
  * A part that can *not* output Base64.
  * @param part
  */
class Part1(part: String) {
  override def toString: String = part
}

object ExtendClassesWithTraits {

  def main(args: Array[String]): Unit = {

    // This is as expected
    val part0 = new Part0("Über den Wolken")
    println(part0.asBase64String())

    // Have the Scala compiler create a new class that inherits from Part1 and AsBase64. Note that this works with
    // third party library classes as well (as long as they are not final).
    val part1 = new Part1("Den Sternen entgegen") with AsBase64
    println(part1.asBase64String())

  }

}
