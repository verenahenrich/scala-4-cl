package com.ibm.s4cl.samples.part07

object ReducingAndFolding {

  def main(args: Array[String]): Unit = {
    val numbers = List(2, 1, 5, 3)

    // reducing lists with reduceLeft
    println(numbers.reduceLeft(_ + _)) // prints '11'
    println(numbers.reduceLeft(_ * _)) // prints '30'
    println(numbers.reduceLeft(_ - _)) // prints '-7'
    println(numbers.reduceLeft(_ min _)) // prints '1'
    println(numbers.reduceLeft(_ max _)) // prints '5'

    // reducing lists with reduceRight
    println(numbers.reduceRight(_ + _)) // prints '11'
    println(numbers.reduceRight(_ * _)) // prints '30'
    println(numbers.reduceRight(_ - _)) // prints '3'
    println(numbers.reduceRight(_ min _)) // prints '1'
    println(numbers.reduceRight(_ max _)) // prints '5'

    // trying to reduce an empty list throws an exception
    //    println(List[Int]().reduceLeft(_ + _)) // throws an exception

    // flatten lists with reduceLeft and reduceRight
    val listOfLists = List(List(1, 2, 3),
                           List(4, 5, 6),
                           List(7, 8, 9),
                           List(10, 11, 12, 13, 14),
                           List(15, 16))
    println(listOfLists.reduceLeft(_ ::: _))
    println(listOfLists.reduceRight(_ ::: _))

    // Since function 'foldLeft(s)' returns a function; variable 'foldLeft1' represents a function which
    // can be called in a next step; to instantiate 'foldLeft1' you either have to make the function
    // type explicit or use partial application (both work fine)
//    val foldLeft1: ((Int, Int) => Int) => Int = numbers.foldLeft(1) // either make function type explicit
    val foldLeft1 = numbers.foldLeft(1) _ // or use partial application
    println(foldLeft1(_ + _)) // prints '12'
    println(foldLeft1(_ * _)) // prints '30'
    println(foldLeft1(_ - _)) // prints '-10'
    println(foldLeft1(_ min _)) // prints '1'
    println(foldLeft1(_ max _)) // prints '5'

    // It is also possible to call foldLeft with all required parameter lists at once
    println(numbers.foldLeft(1)(_ + _)) // prints '11'
    println(numbers.foldLeft(1)(_ * _)) // prints '30'
    println(numbers.foldLeft(1)(_ - _)) // prints '-10'
    println(numbers.foldLeft(1)(_ min _)) // prints '1'
    println(numbers.foldLeft(1)(_ max _)) // prints '5'

    // Folding lists with foldRight and a start value
    println(numbers.foldRight(1)(_ + _)) // prints '12'
    println(numbers.foldRight(1)(_ * _)) // prints '30'
    println(numbers.foldRight(1)(_ - _)) // prints '4'
    println(numbers.foldRight(1)(_ min _)) // prints '1'
    println(numbers.foldRight(1)(_ max _)) // prints '5'

    // Folding an empty list returns the start value
    println(List[Int]().foldLeft(1)(_ + _)) // prints the start value '1'
  }

}
