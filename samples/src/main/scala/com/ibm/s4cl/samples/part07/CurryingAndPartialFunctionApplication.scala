package com.ibm.s4cl.samples.part07

object CurryingAndPartialFunctionApplication {

  // 'multiply' takes three parameters; Its type is (Int, Int, Int) => Int
  def multiply(a: Int, b: Int, c: Int): Int = a * b * c
  multiply(1, 2, 3) // result: 6

  // 'multiplyCurried' is the curried version of 'multiply'
  def multiplyCurried(a: Int): (Int => Int => Int) =
    (b: Int) => (c: Int) => a * b * c

  // 'multiplyCurried' can be simplified to a function with multiple parameter lists
  def multiplyCurriedSimplified(a: Int)(b: Int)(c: Int): Int = a * b * c

  // Partial application of function 'mult3Ints' results in a function where the first argument is
  // bound; The type of 'mult3Ints' is (Int, Int, Int) => Int
  def mult3Ints(a: Int, b: Int, c: Int): Int = a * b * c

  // 'mult2Ints' takes two arguments, i.e., its type is (Int, Int) => Int
  val mult2Ints = (b: Int, c: Int) => mult3Ints(1, b, c)
  mult2Ints(2, 3) // result: 6

  // Shorter alternative: use the underscore
  val mult2IntsShorter = multiply(1, _: Int, _: Int) // equivalent
  mult2IntsShorter(2, 3) // result: 6

  // Calling the following curried 'add' function with only one parameter list returns a function
  // of type Int => Int
  def add(a: Int)(b: Int): Int = a + b

  // Let's store this resulting function to a variable
//  val add1 = add(1) // this line does not compile due to a missing argument list
  // Two plausible ways around this compilation exception
  val add1 = add(1) _ // use partial function application
  val add5: Int => Int = add(5) // make the function type explicit

  // The variables can then be called like regular Int => Int functions
  add1(10) // result: 11
  add5(10) // result: 15

  // Currying can help with type inference
  // In the following code the compiler complains about a missing parameter type
//  satisfy(List(2, 1, 5, 0, 7, -1, 3), 2, _ > 0) // this does not compile
  // We would have to specify the type information explicitly
  satisfy[Int](List(2, 1, 5, 0, 7, -1, 3), 2, _ > 0)
  satisfy(List(2, 1, 5, 0, 7, -1, 3), 2, (_: Int) > 0)

  // With currying, the compiler is able to infer types correctly
  satisfyCurried(List(2, 1, 5, 0, 7, -1, 3))(2, _ > 0)

  // the 'satisfy' function from HigherOrderFunctions.scala again
  def satisfy[T](list: List[T], index: Int, p: T => Boolean): Boolean = p(list(index))

  // the 'satisfy' function in a curried variant
  def satisfyCurried[T](list: List[T])(index: Int, p: T => Boolean): Boolean = p(list(index))

  def animalsToString(dogs: String*)(cats: String*): String =
    s"${dogs.size} dogs (${dogs.mkString(", ")}) and " +
      s"${cats.size} cats (${cats.mkString(", ")})"

  animalsToString("Bello", "Rocky", "Idefix")("Kitty")
    // result: "3 dogs (Bello, Rocky, Idefix) and 1 cats (Kitty)"

  def languageIso(language: String)(implicit default: String): String =
    language match {
      case "English" => "EN"
      case "German" => "DE"
      case _ => default
    }

  languageIso("English")("undefined")
  languageIso("German")("undefined")
  languageIso("French")("undefined")

  implicit val isoDefault = "language is not supported"
  languageIso("English")
  languageIso("German")
  languageIso("French")
}
