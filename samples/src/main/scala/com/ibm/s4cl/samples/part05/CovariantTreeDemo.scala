package com.ibm.s4cl.samples.part05

object CovariantTreeDemo {

  sealed class BinaryTree[+A] {
    // Doesn't compile because T occurs in contravariant
    // position.
//    def setX(x: A): Unit = {}
  }


}

