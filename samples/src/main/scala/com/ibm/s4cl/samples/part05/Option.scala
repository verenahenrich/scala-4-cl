package com.ibm.s4cl.samples.part05

import java.util.{Calendar, Date}

sealed abstract class MyOption[+T] {
  def isEmpty: Boolean
  def get: T
  def getOrElse[S >: T](alt: S): S = if (isEmpty) alt else get
}
final case class MySome[T](value: T) extends MyOption[T] {
  override def isEmpty: Boolean = false
  override def get: T = value
}
final case object MyNone extends MyOption {
  override def isEmpty: Boolean = true
  override def get: Nothing = throw new NoSuchElementException("Calling None.get")
}

object OptionDemo {

  def weekend(): MyOption[String] = {
    val today = Calendar.getInstance()
    today.setTime(new Date())
    today.get(Calendar.DAY_OF_WEEK) match {
      case Calendar.SUNDAY => MySome("Sunday")
      case Calendar.SATURDAY => MySome("Saturday")
      case _ => MyNone
    }
  }

  def main(args: Array[String]): Unit = {
    weekend() match {
      case MySome(weekendDay) => println(s"Yeah, today is $weekendDay.")
      case MyNone => println("It's not the weekend.")
    }

    abstract class Bird
    case class Penguin(name: String) extends Bird
    case class Albatross(name: String, genus: String) extends Bird

    val gentoo = Penguin("Gentoo")
    val northernRoyal = Albatross("Northern Royal", "Diomedea")

    val gentooOpt = MySome(gentoo)
    val birdOpt: MyOption[Bird] = gentooOpt
    val someBird = gentooOpt.getOrElse(northernRoyal)
    println(s"someBird's type: ${someBird.getClass}")
    // Doesn't compile because someBird is a generic Bird.
//    println(someBird.name)
    }
}
