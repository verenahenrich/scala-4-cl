package com.ibm.s4cl.samples.part05

// Override examples
abstract class Document {
  def getAuthor = "unknown"
  // abstract method declaration
  def getTitle: String
}
trait JsonExport {
  // abstract method in trait
  def toJson: String
}
class Tweet extends Document with JsonExport {
  // Concrete override requires modifier
  override def getAuthor: String = "Twitter author"
  // When implementing abstract methods
  // (from traits or abstract classes),
  // the override declaration is optional.
  override def toJson: String = "{ \"json\" : \"output\" }"
  override def getTitle: String = "A Tweet"
}

// Default and named paramters
object JsonFormatter {
  def format(inputJson: String, indent: Int = 2, longOutput: Boolean = false) = ""
  def main(args: Array[String]): Unit = {
    format("{}")
    format("{}", 4, longOutput = true)
    format("{}", longOutput = true, indent = 4)
    format("{}", longOutput = true)
  }
}

object OverloadingAndDefaultParams {

  def main(args: Array[String]): Unit = {

  }

}

class Person {
  def firstName = "First"
  def lastName() = "Last"
}

object Person {
  def main(args: Array[String]): Unit = {
    val p = new Person
    println(p.firstName)
    println(p.lastName)
    // Does not compile
//    println(p.firstName())
    println(p.lastName())
  }
}


