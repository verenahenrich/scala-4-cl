package com.ibm.s4cl.samples.part01

import java.util.regex.Pattern

/**
  * Demonstrate Scala/Java interoperability
  */
object JavaInterop {

  def main(args: Array[String]): Unit = {

    // Use Java classes in Scala code
    val pattern = Pattern.compile("[a-zA-Z]+")
    val input = "This is a test."
    val matcher = pattern.matcher(input)
    while (matcher.find()) {
      println(s"Match: ${input.substring(matcher.start(), matcher.end())}")
    }

    // Automatically convert between Java and Scala collections
    import collection.JavaConversions._
    val list: java.util.List[Int] = new java.util.ArrayList[Int]()
    list.add(1)
    list.add(2)
    list.add(3)
    list.foreach(println)


  }

}
