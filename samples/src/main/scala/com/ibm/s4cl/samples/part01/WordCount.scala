package com.ibm.s4cl.samples.part01

import java.io.File

import scala.io.Source


object WordCount {

  def main(args: Array[String]): Unit = {
    val inputFiles = new File("samples/src/main/resources/").listFiles()
    val wordCount = inputFiles
      .map(Source.fromFile)
      .flatMap(_.getLines())
      .flatMap(_.split("[ \t\n\r\f,.:;?!\\[\\]'\"(){}]+|--"))
      .map(_.toLowerCase())
      .map(_ -> 1)
      .groupBy(identity)
      .mapValues(_.length)
  }

}
