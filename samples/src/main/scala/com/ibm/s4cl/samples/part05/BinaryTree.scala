package com.ibm.s4cl.samples.part05

sealed trait BinaryTree[T] {
  // Use pattern matching on trees
  def countLeaves(): Int = {
    this match {
      case Leaf(_) => 1
      case Branch(left, right) => left.countLeaves + right.countLeaves
    }
  }
}
final case class Leaf[T](terminal: T) extends BinaryTree[T]
final case class Branch[T](left: BinaryTree[T], right: BinaryTree[T]) extends BinaryTree[T]

object TreeDemo {

  def main(args: Array[String]): Unit = {

    // Create a tree with the automatically generated factory methods.
    val tree = Branch(Leaf("John"), Branch(Leaf("loves"), Branch(Leaf("his"), Leaf("cat"))))
    // Print the tree using the automatically generated toString method
    println(tree)

    // Access constructor parameters as public members
    val subject = tree.left
    println(s"The subject: $subject")


    println(s"Tree has ${tree.countLeaves} leaves.")

    // Define a generic method that can handle any tree type parameter
    def getFirstLeaf[T](tree: BinaryTree[T]): T = {
      tree match {
        case Leaf(value) => value
        case Branch(left, _) => getFirstLeaf(left)
      }
    }
    println(s"First leaf is: ${getFirstLeaf(tree)}")


    val firstLeaves = List(tree)
      .map(getFirstLeaf)
    println(s"First leaves of trees in list: $firstLeaves")

    ///////////////////////////////////////////////////////////
    // Our BinaryTree is invariant wrt its type parameter

    // This is fine
    val okTree: BinaryTree[String] = Leaf[String]("string")

    // Doesn't compile, BinaryTree is not covariant
//    def someFun(t: BinaryTree[Any]): Unit = {}
//    someFun(okTree)

    // Doesn't compile, BinaryTree is not covariant
//    val anyTree: BinaryTree[Any] = Leaf[String]("string")

    // Doesn't compile, BinaryTree is not contravariant
//    val stringTree: BinaryTree[String] = Leaf[Any]("string")



  }
}

