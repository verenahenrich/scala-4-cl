package com.ibm.s4cl.samples.part07

object HigherOrderFunctions {

  // Example function which takes another function as a parameter;
  // The parameter 'p' is a function of type 'Int => Boolean'
  def filter(list: List[Int], p: Int => Boolean): List[Int] =
    list.filter(p)

  // Example function which takes another function as a parameter;
  // The parameter 'p' is a function of type 'Int => Boolean'
  def satisfy(list: List[Int], index: Int, p: Int => Boolean): Boolean =
    p(list(index))

  // Example function which returns a function;
  // The return type is a function of type 'String => String'
  def languageIso(default: String): (String => String) =
    (language: String) => language match {
      case "English" => "EN"
      case "German" => "DE"
      case _ => default
    }

  // Example function with multiple parameter lists (similar to 'languageIso')
  def languageIsoMPL(default: String)(language: String): String =
    language match {
      case "English" => "EN"
      case "German" => "DE"
      case _ => default
    }

  // Example of a purely abstract higher-order function: function composition (there is a built-in function that does
  // that)
  def compose[A, B, C](f: A => B, g: B => C): A => C = {
    (a: A) => g(f(a))
  }

  def main(args: Array[String]): Unit = {

    // calling functions that take a function as a parameter
    filter(List(2, 1, 5, 0, 7, -1, 3), _ > 0)
    satisfy(List(2, 1, 5, 0, 7, -1, 3), 2, _ > 0)

    // Since function 'languageIso' returns a function of type 'String => String', variable
    // 'langFunction' represents a function of type 'String => String'
    val langFunction: (String => String) = languageIso("undefined")
    println(langFunction("English"))
    println(langFunction("German"))
    println(langFunction("French"))

    // It is also possible to call 'languageIso' with all required parameter lists at once
    println(languageIso("undefined")("English"))
    println(languageIso("undefined")("German"))
    println(languageIso("undefined")("French"))

    // Equivalent calls for function 'languageIsoMPL', which takes two parameter lists
    val langFunctionMPL: (String => String) = languageIsoMPL("undefined")
    println(langFunctionMPL("English"))
    println(langFunctionMPL("German"))
    println(langFunctionMPL("French"))
    println(languageIsoMPL("undefined")("English"))
    println(languageIsoMPL("undefined")("German"))
    println(languageIsoMPL("undefined")("French"))

  }

}
