package com.ibm.s4cl.samples.part05

object ListsAreCovariant {

  def main(args: Array[String]): Unit = {

    val stringList: List[String] = List("u", "v", "w")

    // Fine because of covariance
    val anyList: List[Any] = stringList
    def someFun(l: List[Any]): Unit = {}

    // Fine because of covariance
    someFun(stringList)
    someFun(anyList)

    val intList = List[Int](1, 2, 3, 42)
    // Fine because of covariance
    val anotherList: List[Any] = stringList ::: intList
  }
}
